package com.dyz.gameserver.pojo;

/**
 * Created by StanWind on 2017/3/21.
 * 南昌麻将 精牌
 */
public class JingPaiVo {
    //  正精
    int zhengJingPai;
    //  负精
    int fuJingPai;
    //  正精下标
    int zhengJingIndex;

    public JingPaiVo() {

    }

    public JingPaiVo(int zhengJingPai, int fuJingPai, int zhengJingIndex) {
        this.zhengJingPai = zhengJingPai;
        this.fuJingPai = fuJingPai;
        this.zhengJingIndex = zhengJingIndex;
    }

    public int getZhengJingIndex() {
        return zhengJingIndex;
    }

    public void setZhengJingIndex(int zhengJingIndex) {
        this.zhengJingIndex = zhengJingIndex;
    }

    public int getZhengJingPai() {
        return zhengJingPai;
    }

    public void setZhengJingPai(int zhengJingPai) {
        this.zhengJingPai = zhengJingPai;
    }

    public int getFuJingPai() {
        return fuJingPai;
    }

    public void setFuJingPai(int fuJingPai) {
        this.fuJingPai = fuJingPai;
    }


}
