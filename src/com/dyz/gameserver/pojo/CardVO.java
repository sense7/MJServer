package com.dyz.gameserver.pojo;

import javax.smartcardio.Card;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kevin on 2016/7/5.
 */
public class CardVO {
    private int cardPoint;

    private int onePoint;//两个吃牌之一

    private int twoPoint;//两个吃牌之二

    private String type;//代表胡的类型（qiangHu）


    public int getOnePoint() {
        return onePoint;
    }

    public void setOnePoint(int onePoint) {
        this.onePoint = onePoint;
    }

    public int getTwoPoint() {
        return twoPoint;
    }

    public void setTwoPoint(int twoPoint) {
        this.twoPoint = twoPoint;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCardPoint() {
        return cardPoint;
    }

    public void setCardPoint(int cardPoint) {
        this.cardPoint = cardPoint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CardVO cardVO = (CardVO) o;

        if (cardPoint != cardVO.cardPoint)
            return false;
        if (cardVO.onePoint == onePoint && cardVO.twoPoint == twoPoint)
            return true;
        else if (cardVO.onePoint == twoPoint && cardVO.twoPoint == onePoint)
            return true;
        return false;
    }

    //自用对比三个点
    public boolean myEquals(CardVO cardVO) {
        List<Integer> b = new ArrayList<>();
        b.add(cardVO.getCardPoint());
        b.add(cardVO.getOnePoint());
        b.add(cardVO.getTwoPoint());

        if (!b.contains(cardPoint))
            return false;
        if (!b.contains(onePoint))
            return false;
        if (!b.contains(twoPoint))
            return false;
        return true;
    }


    public static CardVO createCardVO(String content) {
        CardVO cardVO = new CardVO();
        String[] c = content.split(":");
        cardVO.setCardPoint(Integer.valueOf(c[0]));
        cardVO.setOnePoint(Integer.valueOf(c[1]));
        cardVO.setTwoPoint(Integer.valueOf(c[2]));
        return cardVO;
    }

    @Override
    public int hashCode() {
        int result = cardPoint;
        result = 31 * result + onePoint;
        result = 31 * result + twoPoint;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CardVO{" +
                "cardPoint=" + cardPoint +
                ", onePoint=" + onePoint +
                ", twoPoint=" + twoPoint +
                '}';
    }

    public static CardVO clone(CardVO cardVO) {
        CardVO c = new CardVO();
        c.setCardPoint(cardVO.getCardPoint());
        c.setOnePoint(cardVO.getOnePoint());
        c.setTwoPoint(cardVO.getTwoPoint());
        return c;
    }
}
