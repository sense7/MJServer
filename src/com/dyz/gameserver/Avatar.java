package com.dyz.gameserver;

import com.dyz.gameserver.commons.session.GameSession;
import com.dyz.gameserver.pojo.AvatarVO;
import com.dyz.gameserver.pojo.CardVO;
import com.dyz.gameserver.pojo.RoomVO;
//import com.dyz.gameserver.sprite.Character;
import com.dyz.gameserver.sprite.base.GameObj;
import com.dyz.gameserver.sprite.tool.AsyncTaskQueue;
import com.dyz.myBatis.services.AccountService;
import com.dyz.persist.util.GlobalUtil;
import com.dyz.persist.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kevin on 2016/6/18.
 */
public class Avatar implements GameObj {
    public AvatarVO avatarVO;
    //请求吃
    public boolean chiQuest = false;
    //存储吃牌的信息
    public List<CardVO> cardVO = new ArrayList<>();
    //储存临时吃牌信息
    public CardVO chiCardVO;
    //请求碰
    public boolean pengQuest = false;
    //请求杠
    public boolean gangQuest = false;
    //请求胡
    public boolean huQuest = false;

    //  请求定缺
    public boolean queQuest = false;

    //当前玩家能否吃
    public boolean canHu = true;

    public boolean qiangHu = true;

    //胡牌类型
    public int huMode = 0;
    /*
     * 胡牌方式 0-天胡 1-地胡 2-杠上开花 3-抢杠胡 4-自摸 5-放炮
     * 返回胡牌类型 0-没有胡牌 1-南昌平胡 2-碰碰胡 3-七小对 4-十三烂 5-七星十三烂
     * 6-德国平胡 7-德国碰碰胡 8-德国七小对 9-德国十三烂 10-德国七星十三烂
     * 11-精吊平胡 12-精吊德国平胡 13-精吊七小对 14-精吊德国七小对 15-精吊碰碰胡 16-精吊德国碰碰胡
    */
    public int huType = 0;
    /**
     * 是否是一局结算时掉线
     */
    public boolean overOff = false;
    /**
     * 存储某一句游戏断线时 结算信息
     */
    public String oneSettlementInfo;


    //当自己摸牌时检测自己是否有杠的牌。把杠的牌放入到整个list里面，然后在转入给前端
    public List<Integer> gangIndex = new ArrayList<Integer>();
    /**
     * 检测到有人胡牌时存储胡牌的详细消息(划水麻将和长沙麻将用)
     * 0: 天胡
     * 1：明杠 	 1*3番
     * 2：暗杠	 2*3番
     * 3：放杠/点杠 	   1*3番（谁放牌谁出番）
     * 4：自摸 	6番（其他三家每家出2番）
     * 5：普通点炮	3番（谁放炮谁出番）
     * 6：七对点炮	3*3番（谁放炮谁出番）
     * 7：七对自摸	18番（其他三家每家出6番）
     * 8：杠开/杠上花	18番（其他三家每家出6番）
     * 9：抢杠	3*3番（谁要杠牌谁出番）
     * 10：一炮双响	根据胡牌者的牌型来计算放炮者出的番数（胡牌两方所胡牌型的番数相加）
     * 11：一炮三响	根据胡牌者的牌型来计算放炮者出的番数（同上）
     * <p>
     * 数组格式   牌索引:类型
     * 放弃的时候需要清空
     * <p>
     * 小胡点炮：3
     * 大胡点炮：3*3
     * <p>
     * 小胡自摸： 2*3   （每家2分）
     * 大胡自摸： 2*3*3  (每家6分)
     */
    public List<String> huAvatarDetailInfo = new ArrayList<String>();


    public List<CardVO> getCardVO() {
        return cardVO;
    }

    public void setCardVO(List<CardVO> cardVO) {
        this.cardVO = cardVO;
    }

    public int[] getChiNotMovePai() {
        int[] chiL = new int[34];
        if (resultRelation.get(4) != null) {
            String strs[] = resultRelation.get(4).split(",");
            for (int i = 0; i < strs.length; i++) {
                String[] temp = strs[i].split(":");
                for (String t : temp) {
                    if (t != null && !t.equals("")) {
                        chiL[Integer.valueOf(t)]++;
                    }
                }
            }
        }
        return chiL;
    }

    public int[] getNotMovePai() {
        int[] chiL = new int[34];
        if (resultRelation.get(4) != null) {
            String strs[] = resultRelation.get(4).split(",");
            for (int i = 0; i < strs.length; i++) {
                String[] temp = strs[i].split(":");
                for (String t : temp) {
                    if (t != null && !t.equals("")) {
                        chiL[Integer.valueOf(t)]++;
                    }
                }
            }
        }
        //加入碰
        if (resultRelation.get(1) != null) {
            String[] pengStrs = resultRelation.get(1).split(",");
            for (String t : pengStrs) {
                if (t != null && !t.equals("")) {
                    chiL[Integer.valueOf(t)] += 3;
                }
            }
        }
        //加入杠
        if (resultRelation.get(2) != null) {
            String[] gangStrs = resultRelation.get(2).split(",");
            for (String t : gangStrs) {
                if (t != null && !t.equals("")) {
                    int cc = chiL[Integer.valueOf(t)];
                    if (cc == 3) {
                        chiL[Integer.valueOf(t)] = 4;
                    } else if (cc == 0) {
                        chiL[Integer.valueOf(t)] = 4;
                    } else {
                        Log.getLogger().error("出错了!吃牌计算加入杠,当前值->" + cc + " 自动最大修正->" + t);
                        chiL[Integer.valueOf(t)] = 4;
                    }
                }
            }
        }
        return chiL;
    }


    /**
     * 增加吃牌
     *
     * @param cardIndex
     * @param a
     * @param b
     */
    public boolean addChiCardVO(int cardIndex, int a, int b) {
        CardVO card = new CardVO();
        card.setCardPoint(cardIndex);
        card.setOnePoint(a);
        card.setTwoPoint(b);

        boolean flag = false;
        if (resultRelation.get(4) == null
                && resultRelation.get(1) == null
                && resultRelation.get(2) == null) {
            flag = true;
        } else {
            //老版本判断
//            //把吃过的全部加入list
//            List<CardVO> chiCardList = new ArrayList<>();
//            for (int i = 0; i < strs.length; i++) {
//                CardVO temp = CardVO.createCardVO(strs[i]);
//                chiCardList.add(temp);
//            }
//            //以当前添加的为模板 计算吃过几组同样的
//            int count = 0;
//            for (CardVO t : chiCardList)
//                if (t.myEquals(card))
//                    count++;
//            //吃过这么多同样的 那么手牌应该至少这么多张+1 当前要吃
//            if (avatarVO.getPaiArray()[0][card.getOnePoint()] > count
//                    || avatarVO.getPaiArray()[0][card.getTwoPoint()] > count)
//                flag = true;
            //把吃过的牌都加进去
            int[] chiL = getNotMovePai();
            Log.getLogger().debug("已经不能动的牌->" + chiL);
            Log.getLogger().debug("自己的手牌。。->" + avatarVO.getPaiArray()[0]);
//            int c1 = avatarVO.getPaiArray()[0][card.getCardPoint()];
            int c2 = avatarVO.getPaiArray()[0][card.getOnePoint()];
            int c3 = avatarVO.getPaiArray()[0][card.getTwoPoint()];
            if (c2 > chiL[card.getOnePoint()] && c3 > chiL[card.getTwoPoint()])
                flag = true;
            Log.getLogger().debug("手牌张数->" + c2 + "---" + c3);
        }

        //可以加入吃
        if (flag)
            cardVO.add(card);
        else
            Log.getLogger().debug("吃重复——>" + card);

        return flag;
    }

    public RoomVO getRoomVO() {
        return roomVO;
    }

    public void setRoomVO(RoomVO roomVO) {
        this.roomVO = roomVO;
        if (avatarVO != null) {
            avatarVO.setRoomId(roomVO.getRoomId());
        }
    }

    private RoomVO roomVO;
    /**
     * session
     */
    private GameSession session;
    /**
     * 牌类别 3-不含字牌，4-含字牌
     */
    private int card_row = 2;
    /**
     * 牌字
     */
    private int card_col = 27;
    /**
     * 日志记录
     */
    private static final Logger logger = LoggerFactory.getLogger(Character.class);
    /**
     * 异步操作队列
     */
    private AsyncTaskQueue asyncTaskQueue = new AsyncTaskQueue();
    /**
     * list里面字符串规则
     * 杠：uuid(出牌家),介绍(明杠，暗杠)  （123，明杠）
     * 自己摸来杠：介绍(明杠，暗杠)
     * <p>
     * 点炮：uuid(出牌家),介绍(胡的类型) （123，qishouhu）
     * 自摸：介绍(胡的类型)
     * <p>
     * 碰：
     * <p>
     * eg:  碰： key:1     value: 碰的牌的下标
     * 杠：key:2    value: 杠的牌的下标
     * 胡：key:3    value: 胡的牌的下标
     * 吃：key:4    value:  吃的牌的下标(1:2:3)
     * <p>
     * key:1:碰    2:杠    3:胡   4:吃   5:抢胡
     * value:信息，分条信息之间用","隔开
     */
    private Map<Integer, String> resultRelation = new HashMap<Integer, String>();

    public Map<Integer, String> getResultRelation() {
        return resultRelation;
    }

    //存储杠和胡的信息
    public void putResultRelation(Integer i, String str) {
        synchronized (resultRelation) {
            if (resultRelation.get(i) == null) {
                resultRelation.put(i, str);
            } else {
                resultRelation.put(i, resultRelation.get(i) + "," + str);
            }
        }
    }

    public Avatar() {
    }


    public void setQuestToFalse() {
        huQuest = false;
        gangQuest = false;
        pengQuest = false;
        chiQuest = false;
        queQuest = false;
    }

    /**
     * 更新用户数据表信息
     *
     * @param value
     */
    public void updateRoomCard(int value) {
        int number = avatarVO.getAccount().getRoomcard();
        number += value;
        avatarVO.getAccount().setRoomcard(number);
        AccountService.getInstance().updateByPrimaryKeySelective(avatarVO.getAccount());
    }

    /**
     * 获取玩家session
     *
     * @return
     */
    public GameSession getSession() {
        return session;
    }

    /**
     * 获取玩家session
     *
     * @return
     */
    public void setSession(GameSession gameSession) {
        this.session = gameSession;
    }

    /**
     * 添加异步任务，针对异步操作数据库
     *
     * @param tasks
     */
    public void addAsyncTask(Runnable... tasks) {
        asyncTaskQueue.addTask(tasks);
    }

    /**
     * 获取用户uuid
     *
     * @return
     */
    public int getUuId() {
        return avatarVO.getAccount().getUuid();
    }


    /**
     * 设置房间属性,然后清理并重新生成新的牌数据
     * 1-转转麻将，2-划水麻将，3-长沙麻将
     */
    public void CreatePaiArray() {
        if (roomVO.getRoomType() == 1) {
            card_col = 27;
            if (roomVO.getHong()) {
                card_col = 34;
            }
        } else if (roomVO.getRoomType() == 2) {
            card_col = 34;

        } else if (roomVO.getRoomType() == 3) {
            card_col = 27;
        } else if (roomVO.getRoomType() == 5) {
            card_col = 34;
        }
        cleanPaiData();
    }

    /**
     * 清理玩家的牌数据
     */
    public void cleanPaiData() {
        if (avatarVO.getPaiArray() == null) {
            avatarVO.setPaiArray(new int[card_row][card_col]);
        }
        for (int i = 0; i < card_row; i++) {
            for (int k = 0; k < card_col; k++) {
                avatarVO.getPaiArray()[i][k] = 0;
            }
        }
    }

    /**
     * 檢測是否可以碰 大于等于2张且对应的下标不为1都可以碰
     *
     * @param cardIndex
     * @return
     */
    public boolean checkPeng(int cardIndex) {

        Log.getLogger().debug("杠了的牌=" + cardIndex + "===resultRelation==" + resultRelation.get(2));
        Log.getLogger().debug("碰了的牌=" + cardIndex + "===resultRelation==" + resultRelation.get(1));
        boolean flag = false;
        Log.getLogger().debug(avatarVO.getAccount() + " peng count--->" + avatarVO.getPaiArray()[0][cardIndex]);


        if (avatarVO.getPaiArray()[0][cardIndex] >= 2) {
            if (resultRelation.get(1) == null && resultRelation.get(4) == null) {
                flag = true;
            } else {
                int[] chiL = getNotMovePai();
                if (avatarVO.getPaiArray()[0][cardIndex] - chiL[cardIndex] >= 2)
                    flag = true;
            }
        }
        return flag;
    }

    /**
     * 檢測是否可以杠别人出的牌/此牌对应的下标不为1（碰过了的牌）
     *
     * @param cardIndex
     * @return
     */
    public boolean checkGang(int cardIndex) {
        boolean flag = false;
        gangIndex.clear();//每次出牌就先清除缓存里面的可以杠的牌下标
        if (avatarVO.getPaiArray()[0][cardIndex] == 3 && avatarVO.getPaiArray()[1][cardIndex] == 0) {
            //if(resultRelation.get(1) ==null){
            int[] movePai = getNotMovePai();
            if (movePai[cardIndex] == 0) {
                Log.getLogger().debug("gang---> index- " + cardIndex);
                gangIndex.add(cardIndex);
                flag = true;
            } else {
                Log.getLogger().debug("不可移动牌占位  不能杠->" + cardIndex);
            }
            /*}else{
                //
        		String strs [] = resultRelation.get(1).split(",");
        		for (int i = 0; i < strs.length; i++) {
        			if(strs[i].equals(cardIndex+"")){
						flag  =  false;
						gangIndex.clear();
						i = strs.length;
					}
					else{
						gangIndex.add(cardIndex);
						flag  =  true;
					}
				}
        	}*/
        }
        return flag;
    }

    /**
     * 检测当前自己的牌是否可杠
     *
     * @param
     * @return
     */
    public boolean checkSelfGang() {
        //system.out.println("杠了的牌==杠家id"+avatarVO.getAccount().getUuid()+"===="+resultRelation.get(2));
        //system.out.println("碰了的牌==杠家id"+avatarVO.getAccount().getUuid()+"===="+resultRelation.get(1));
        gangIndex.clear();//先清除缓存里面的可以杠的牌下标
        //剔除掉当前以前吃，碰，杠的牌组 再进行比较
        boolean flag = false;
        if (!roomVO.isAddWordCard()) {
            //划水麻将没有风牌  就27
            for (int i = 0; i < 27; i++) {
                if (avatarVO.getPaiArray()[0][i] == 4 && avatarVO.getPaiArray()[1][i] != 2) {
                    //先判断所有4个的牌组中是否有未杠过的
                    gangIndex.add(i);
                    flag = true;
                    break;//多个杠的情况下默认杠下标最小的个牌
                    /*if(resultRelation.get(2) == null ){
                        gangIndex.add(i);
    					flag =  true;
    					i = 100;
    				}
    				else if(resultRelation.get(2) != null ){
    					String strs [] = resultRelation.get(2).split(",");
    					for (int j = 0; j < strs.length; j++) {
    						if(strs[j].equals(i+"")){
    							flag =  false;
    							gangIndex.clear();
    							j = 100;
    							i = 100;
    						}
    						else{
    							gangIndex.add(i);
    							flag =  true;
    						}
    					}
    					for (int j = 0; j < strs.length; j++) {
    						if(!strs[j].equals(i+"")){
    							gangIndex.add(i);
    						}
    					}
    				}*/
                }
            }
        } else {
            //划水麻将有风牌  就 34
            for (int i = 0; i < 34; i++) {
                if (avatarVO.getPaiArray()[0][i] == 4
                        && avatarVO.getPaiArray()[1][i] != 2 && avatarVO.getPaiArray()[1][i] != 4) {
                    //先判断所有4个的牌组中是否有未杠过的
                    gangIndex.add(i);
                    flag = true;
                    break;//多个杠的情况下默认杠下标最小的个牌
                    /*if(resultRelation.get(2) == null ){
                        gangIndex.add(i);
    					flag =  true;
    					i = 100;//多个杠的情况下默认杠下标最小的个牌
    				}
    				else if(resultRelation.get(2) != null ){
    					String strs [] = resultRelation.get(2).split(",");
    					for (int j = 0; j < strs.length; j++) {
    						if(strs[j].equals(i+"")){
    							flag =  false;
    							gangIndex.clear();
    							j = 100;
    							i = 100;
    						}
    						else{
    							gangIndex.add(i);
    							flag =  true;
    						}
    					}
    					for (int j = 0; j < strs.length; j++) {
    						if(!strs[j].equals(i+"")){
    							gangIndex.add(i);
    						}
    					}
    				}*/
                }
            }
        }
        return flag;
    }

    /**
     * 檢測是否可以吃
     *
     * @param cardIndex
     * @return
     */
    public boolean checkChi(int cardIndex) {

        boolean flag = false;
        //南昌麻将吃法
        //system.out.println("判断吃否可以吃牌-----cardIndex:"+cardIndex);


        /**
         * 这里检测吃的时候需要踢出掉碰 杠了的牌****
         */
        cardVO = new ArrayList<>(); //清空上次计算

        int[] cardList = avatarVO.getPaiArray()[0];

        if (cardIndex >= 0 && cardIndex <= 8) {
            //一 二三万
            if (cardIndex == 0 && cardList[1] >= 1 && cardList[2] >= 1) {
                flag = addChiCardVO(cardIndex, 1, 2);
            }

            //二
            if (cardIndex == 1) {
                if (cardList[0] >= 1 && cardList[2] >= 1) {
                    flag = addChiCardVO(cardIndex, 0, 2);
                }
                if (cardList[3] >= 1 && cardList[2] >= 1) {
                    flag = addChiCardVO(cardIndex, 2, 3);
                }
            }

            //九
            if (cardIndex == 8 && cardList[7] >= 1 && cardList[6] >= 1) {
                flag = addChiCardVO(cardIndex, 6, 7);
            }

            //八
            if (cardIndex == 7) {
                if (cardList[8] >= 1 && cardList[6] >= 1) {
                    flag = addChiCardVO(cardIndex, 6, 8);
                }
                if ((cardList[5] >= 1 && cardList[6] >= 1)) {
                    flag = addChiCardVO(cardIndex, 5, 6);
                }
            }

            //三 - 七
            if (cardIndex >= 2 && cardIndex <= 6) {
                if ((cardList[cardIndex - 1] >= 1 && cardList[cardIndex + 1] >= 1)) {
                    flag = addChiCardVO(cardIndex, cardIndex - 1, cardIndex + 1);
                }
                if (cardList[cardIndex - 1] >= 1 && cardList[cardIndex - 2] >= 1) {
                    flag = addChiCardVO(cardIndex, cardIndex - 1, cardIndex - 2);

                }
                if (cardList[cardIndex + 1] >= 1 && cardList[cardIndex + 2] >= 1) {
                    flag = addChiCardVO(cardIndex, cardIndex + 1, cardIndex + 2);
                }
            }

            //万结束
        } else if ((cardIndex >= 9 && cardIndex <= 17)) {
            //一 二三条
            if (cardIndex == 9 && cardList[10] >= 1 && cardList[11] >= 1) {
                flag = addChiCardVO(cardIndex, 10, 11);

            }

            //二
            if (cardIndex == 10) {
                if (cardList[9] >= 1 && cardList[11] >= 1) {
                    flag = addChiCardVO(cardIndex, 9, 11);

                }
                if (cardList[11] >= 1 && cardList[12] >= 1) {
                    flag = addChiCardVO(cardIndex, 11, 12);
                }
            }

            //九
            if (cardIndex == 17 && cardList[16] >= 1 && cardList[15] >= 1) {
                flag = addChiCardVO(cardIndex, 16, 15);
            }

            //八
            if (cardIndex == 16) {
                if (cardList[17] >= 1 && cardList[15] >= 1) {
                    flag = addChiCardVO(cardIndex, 17, 15);
                }
                if ((cardList[14] >= 1 && cardList[15] >= 1)) {
                    flag = addChiCardVO(cardIndex, 14, 15);
                }
            }

            //三 - 七
            if (cardIndex >= 11 && cardIndex <= 15) {
                if ((cardList[cardIndex - 1] >= 1 && cardList[cardIndex + 1] >= 1)) {
                    flag = addChiCardVO(cardIndex, cardIndex - 1, cardIndex + 1);
                }
                if (cardList[cardIndex - 1] >= 1 && cardList[cardIndex - 2] >= 1) {
                    flag = addChiCardVO(cardIndex, cardIndex - 1, cardIndex - 2);
                }
                if (cardList[cardIndex + 1] >= 1 && cardList[cardIndex + 2] >= 1) {
                    flag = addChiCardVO(cardIndex, cardIndex + 1, cardIndex + 2);
                }
            }

            //条结束
        } else if (cardIndex >= 18 && cardIndex <= 26) {
            //一 二三筒
            if (cardIndex == 18 && cardList[19] >= 1 && cardList[20] >= 1) {
                flag = addChiCardVO(cardIndex, 19, 20);
            }

            //二
            if (cardIndex == 19) {
                if (cardList[18] >= 1 && cardList[20] >= 1) {
                    flag = addChiCardVO(cardIndex, 18, 20);
                }
                if (cardList[20] >= 1 && cardList[21] >= 1) {
                    flag = addChiCardVO(cardIndex, 20, 21);
                }
            }

            //九
            if (cardIndex == 26 && cardList[25] >= 1 && cardList[24] >= 1) {
                flag = addChiCardVO(cardIndex, 25, 24);
            }

            //八
            if (cardIndex == 25) {
                if (cardList[24] >= 1 && cardList[26] >= 1) {
                    flag = addChiCardVO(cardIndex, 24, 26);
                }
                if ((cardList[23] >= 1 && cardList[24] >= 1)) {
                    flag = addChiCardVO(cardIndex, 23, 24);
                }
            }

            //三 - 七
            if (cardIndex >= 20 && cardIndex <= 24) {
                if ((cardList[cardIndex - 1] >= 1 && cardList[cardIndex + 1] >= 1)) {
                    flag = addChiCardVO(cardIndex, cardIndex - 1, cardIndex + 1);
                }
                if (cardList[cardIndex - 1] >= 1 && cardList[cardIndex - 2] >= 1) {
                    flag = addChiCardVO(cardIndex, cardIndex - 1, cardIndex - 2);
                }
                if (cardList[cardIndex + 1] >= 1 && cardList[cardIndex + 2] >= 1) {
                    flag = addChiCardVO(cardIndex, cardIndex + 1, cardIndex + 2);
                }
            }

            //筒结束
        } else if (cardIndex >= 27 && cardIndex <= 30) {
            //大于1的字牌有两张
            int aIndex = -1;
            int bIndex = -1;

            int count = 0;
            for (int i = 0; i < 4; i++) {
                int tempIndex = 27 + i;
                //跳过自己相同的牌
                if (tempIndex == cardIndex)
                    continue;

                if (cardList[tempIndex] >= 1) {
                    if (aIndex == -1)
                        aIndex = tempIndex;
                    else
                        bIndex = tempIndex;

                    if (bIndex != -1) {
                        count++;
                        flag = addChiCardVO(cardIndex, aIndex, bIndex);
                        if (count == 2) {
                            //说明手里有四张风牌 就有三种吃法 by stan
                            flag = addChiCardVO(cardIndex, bIndex, bIndex - 1 == cardIndex ? bIndex - 2 : bIndex - 1);
                        }
                        bIndex = -1;
                    }
                }
            }


        } else if (cardIndex >= 31 && cardIndex <= 33) {
            //大于1的字牌有两张
            int aIndex = -1;
            int bIndex = -1;
            for (int i = 0; i < 3; i++) {
                int tempIndex = 31 + i;
                //跳过自己相同的牌
                if (tempIndex == cardIndex)
                    continue;

                if (cardList[tempIndex] >= 1) {
                    if (aIndex == -1) {
                        aIndex = tempIndex;
                    } else {
                        bIndex = tempIndex;
                        break;
                    }
                }
            }

            if (aIndex != -1 && bIndex != -1) {
                flag = addChiCardVO(cardIndex, aIndex, bIndex);
            }
        }


        return flag;
    }

    /**
     * 为自己的牌组里加入新牌
     * /碰 1  杠2  胡3  吃4
     *
     * @param cardIndex
     */
    public boolean putCardInList(int cardIndex) {
        if (avatarVO.getPaiArray()[0][cardIndex] < 4) {
            avatarVO.getPaiArray()[0][cardIndex]++;
            return true;
        } else {
            //再没检测出为什么牌组里面已经有4张牌的错误消息前暂且注释掉
            //System.out.println("Error : putCardInList --> 牌数组里已经有4张牌");
            //try {  
            //session.sendMsg(new ErrorResponse(ErrorCode.Error_000008));
            // } catch (IOException e) {
            //    e.printStackTrace();
            // }
            return true;
        }
    }

    /**
     * 设置牌的状态
     *
     * @param cardIndex
     * @param type;//碰  1  杠2  胡3  吃4
     */
    public void setCardListStatus(int cardIndex, int type) {
        avatarVO.getPaiArray()[1][cardIndex] = type;
    }

    /**
     * 把牌从自己的牌列中去除
     *
     * @param cardIndex
     */
    public void pullCardFormList(int cardIndex) {
        if (avatarVO.getPaiArray()[0][cardIndex] > 0) {
            avatarVO.getPaiArray()[0][cardIndex]--;
        } else {/*
            try {
                session.sendMsg(new ErrorResponse(ErrorCode.Error_000007));
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Error : pullCardFormList --> 牌数组里没有这张牌");
        */
        }
    }

    /**
     * 得到牌组的一维数组。用来判断是否胡牌和听牌用
     *
     * @return
     */
    public int[][] getPaiArray() {
        return avatarVO.getPaiArray();
    }

    public int[] getSinglePaiArray() {
        int[] result = GlobalUtil.CloneIntList(avatarVO.getPaiArray()[0]);
        return result;
    }

    public String printPaiString() {
        String sb = "";
        for (int a = 0; a < 2; a++) {
            sb += "[";
            for (int i = 0; i < avatarVO.getPaiArray()[a].length; i++) {
                sb += avatarVO.getPaiArray()[a][i] + ",";
            }
            sb += "]";
        }
        return sb;
    }

    @Override
    public void destroyObj() {
        //统计在线用户****
        logger.info("Avatar用户{}断开服务器链接", avatarVO.getAccount().getNickname());
        if (session != null) {
            //session.clearAllInfo();
        }
    }

    /**
     * kevinTest
     * 测试胡牌用的数据
     *
     * @return
     */
    public int[] getTestPaiArray() {
        return new int[]{0, 2, 2, 2, 4, 1, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    }

    /**
     * 测试用设置牌
     * StanWind
     */
    public void setTestPaiArray() {
        int[][] a = {{0, 0, 2, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
        avatarVO.setPaiArray(a);
    }


}
