package com.dyz.gameserver.msg.processor.location;

import com.dyz.gameserver.Avatar;
import com.dyz.gameserver.commons.message.ClientRequest;
import com.dyz.gameserver.commons.session.GameSession;
import com.dyz.gameserver.msg.processor.common.MsgProcessor;
import com.dyz.persist.util.Log;

/**
 * Created by StanWind on 2017/5/3.
 */
public class LocationDesMsgProcessor extends MsgProcessor {
    @Override
    public void process(GameSession gameSession, ClientRequest request) throws Exception {
        Avatar avatar = gameSession.getRole(Avatar.class);
        avatar.avatarVO.setLocationName(request.getString());
        Log.getLogger().info(avatar.getUuId() + " 设置位置->" + avatar.avatarVO.getLocationName());
    }
}
