package com.dyz.gameserver.msg.processor.dingque;

//import org.junit.Test;

import com.dyz.context.ErrorCode;
import com.dyz.gameserver.Avatar;
import com.dyz.gameserver.commons.message.ClientRequest;
import com.dyz.gameserver.commons.session.GameSession;
import com.dyz.gameserver.msg.processor.common.MsgProcessor;
import com.dyz.gameserver.msg.response.ErrorResponse;
import com.dyz.gameserver.pojo.AvatarVO;
import com.dyz.persist.util.StringUtil;


public class DingQueMsgProcessor extends MsgProcessor {

	@Override
	public void process(GameSession gameSession, ClientRequest request)
			throws Exception {
		
		String queType = request.getString();
		
	
		if(StringUtil.isInteger(queType, 0, 0))
		{
			
			int type = Integer.parseInt(queType);
			Avatar avatar = new Avatar();
			AvatarVO avatarvo  = new AvatarVO(); 
			avatarvo.setQueType(type);
			avatar.avatarVO = avatarvo;
			
		}
		else
		{
			gameSession.sendMsg(new ErrorResponse(ErrorCode.Error_000019));
		}
		
	}
	
	

	
	

}


