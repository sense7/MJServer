package com.dyz.gameserver.msg.processor.feedback;

import com.dyz.gameserver.Avatar;
import com.dyz.gameserver.commons.message.ClientRequest;
import com.dyz.gameserver.commons.session.GameSession;
import com.dyz.gameserver.msg.processor.common.MsgProcessor;
import com.dyz.gameserver.msg.response.messageBox.MessageBoxResponse;
import com.dyz.myBatis.model.Feedback;
import com.dyz.myBatis.services.FeedbackService;
import com.dyz.persist.util.JsonUtilTool;

import java.util.Date;

/**
 * Created by StanWind on 2017/5/16.
 */
public class FeedbackProcessor extends MsgProcessor {
    @Override
    public void process(GameSession gameSession, ClientRequest request) throws Exception {
        String message = request.getString();
        System.err.print(message);
        Feedback feedback = JsonUtilTool.fromJson(message, Feedback.class);
        int uuid = gameSession.getRole(Avatar.class).getUuId();
        if (null == feedback.getComment() || null == feedback.getTitle()
                || "".equals(feedback.getComment()) || "".equals(feedback.getTitle())) {
            gameSession.sendMsg(new MessageBoxResponse("请完善输入信息"));
            return;
        }


        if (uuid != 0 && uuid == feedback.getUuid()) {
            //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            feedback.setCreatetime(new Date());
            int id = FeedbackService.getInstance().insert(feedback);
            if (id > 0) {
                gameSession.sendMsg(new MessageBoxResponse("我们已收到您的反馈，感谢"));
            }
        }
    }
}
