package com.dyz.gameserver.msg.response.dingque;

import java.io.IOException;

import com.dyz.context.ConnectAPI;
import com.dyz.gameserver.commons.message.ServerResponse;

public class DingQueResponse extends ServerResponse{

	public DingQueResponse(int status, int queType) {
		super(status, ConnectAPI.DINGQUE_RESPONSE);

        if(status > 0)
		{
//			JSONObject json = new JSONObject();
//			json.put("queType", queType+"");
			try {
				output.writeUTF(queType+"");
			} catch (IOException e) {
				e.printStackTrace();
			}
			finally
			{
				output.close();
			}
		}
		
		
	}

}
