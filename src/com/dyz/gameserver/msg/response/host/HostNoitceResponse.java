package com.dyz.gameserver.msg.response.host;

import com.dyz.context.ConnectAPI;
import com.dyz.gameserver.commons.message.ServerResponse;

import java.io.IOException;

/**
 * 
 * @author luck
 * 后台更新公告后发送给前段
 *
 */
public class HostNoitceResponse extends ServerResponse {
	public HostNoitceResponse(int status, String noitce) {
        super(status, ConnectAPI.HOST_SEND_RESPONSE);
        if(status >0){
            try {
            	
                output.writeUTF(noitce);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
           	 output.close();
			}
        }
       // entireMsg();
    }
}
