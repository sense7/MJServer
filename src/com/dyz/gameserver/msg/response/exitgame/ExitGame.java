package com.dyz.gameserver.msg.response.exitgame;

import com.dyz.context.ConnectAPI;
import com.dyz.gameserver.commons.message.ServerResponse;

import java.io.IOException;

/**
 * Created by hailian on 17/2/14.
 */
public class ExitGame extends ServerResponse {
    /**
     * 必须调用此方法设置消息号
     *
     * @param status
     * @param msgCode
     */
    public ExitGame(int status, int msgCode) {
        super(status, ConnectAPI.EXITGAME_RESPONE);

        if(status > 0)
        {

            //0 退出成功 1退出失败
            String str = String.valueOf(msgCode);
            System.out.print("退出游戏"+str);
            try {
                output.writeUTF(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
