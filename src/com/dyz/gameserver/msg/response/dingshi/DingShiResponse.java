package com.dyz.gameserver.msg.response.dingshi;

import java.io.IOException;

import com.dyz.context.ConnectAPI;
import com.dyz.gameserver.commons.message.ServerResponse;

public class DingShiResponse extends ServerResponse{

	public DingShiResponse(int status, int second) {
		super(status, ConnectAPI.DINGSHI_RESPONSE);

        if(status>0)
		{
			try {
				output.writeUTF(second+"");
			} catch (IOException e) {
				e.printStackTrace();
			}
			finally
			{
				output.close();
			}
		}
	}

}
