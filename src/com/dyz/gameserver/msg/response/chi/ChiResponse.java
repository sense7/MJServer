package com.dyz.gameserver.msg.response.chi;

import com.dyz.context.ConnectAPI;
import com.dyz.gameserver.commons.message.ServerResponse;
import com.dyz.gameserver.pojo.CardVO;
import net.sf.json.JSONObject;

import java.io.IOException;

//import org.junit.Test;

/**
 * 
 * @author luck
 *
 */
public class ChiResponse extends ServerResponse {
    /**
     * 必须调用此方法设置消息号
     * @param status
     * @param cardVO
     * @param AvatarId
     */
    //2017年03月27日11:03:07 改造
    public ChiResponse(int status, CardVO cardVO, int AvatarId) {
        super(status, ConnectAPI.CHIPAI_RESPONSE);
        if(status >0){
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("avatarId", AvatarId);
                jsonObject.put("cardPoint", cardVO.getCardPoint());
                jsonObject.put("onePoint", cardVO.getOnePoint());
                jsonObject.put("twoPoint", cardVO.getTwoPoint());
                output.writeUTF(jsonObject.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //entireMsg();
    }
    
  
}
