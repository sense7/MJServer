package com.dyz.gameserver.msg.response.saizipoint;

import com.dyz.context.ConnectAPI;
import com.dyz.gameserver.commons.message.ServerResponse;

import java.io.IOException;

/**
 * Created by hailian on 17/2/14.
 */
public class saizipoint extends ServerResponse{
    /**
     * 必须调用此方法设置消息号
     *
     * @param status
     * @param msgCode
     */
    public saizipoint(int status, String str) {
        super(status, ConnectAPI.SAIZIPOINT_RESPONE);
        if(status>0)
        {
            try {
                output.writeUTF(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }
}
