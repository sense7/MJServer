package com.dyz.gameserver.msg.response.jingpai;

import com.dyz.context.ConnectAPI;
import com.dyz.gameserver.commons.message.ServerResponse;
import com.dyz.gameserver.pojo.JingPaiVo;
import com.dyz.persist.util.JsonUtilTool;

import java.io.IOException;

/**
 * Created by StanWind on 2017/3/21.
 */
public class JingPaiResponse extends ServerResponse {

    public JingPaiResponse(int status, JingPaiVo jingPaiVo) {
        super(status, ConnectAPI.JINGPAI_RESPONSE);
        if (status > 0) {
            try {
                //格式
                output.writeUTF(JsonUtilTool.toJson(jingPaiVo));
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                output.close();
            }
        }
        //entireMsg();
    }
}
