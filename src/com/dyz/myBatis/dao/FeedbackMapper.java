package com.dyz.myBatis.dao;

import com.dyz.myBatis.model.Feedback;

public interface FeedbackMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Feedback record);

    Feedback selectByPrimaryKey(Integer id);
}