package com.dyz.myBatis.dao;

import com.dyz.myBatis.model.GameTimes;

public interface GameTimesMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GameTimes record);

    int insertSelective(GameTimes record);


    GameTimes selectByUUID(Integer uuid);

    int updateByUUID(GameTimes record);

}