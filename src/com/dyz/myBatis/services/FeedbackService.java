package com.dyz.myBatis.services;

import com.dyz.myBatis.dao.FeedbackMapper;
import com.dyz.myBatis.daoImp.FeedbackDaoImp;
import com.dyz.myBatis.model.Feedback;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * Created by StanWind on 2017/5/16.
 */
public class FeedbackService {
    private FeedbackMapper feedbackMapper;

    private static FeedbackService feedbackService = new FeedbackService();

    public static FeedbackService getInstance() {
        return feedbackService;
    }

    public void initSetSession(SqlSessionFactory sqlSessionFactory) {
        feedbackMapper = new FeedbackDaoImp(sqlSessionFactory);
    }


    public int insert(Feedback record) {
        return feedbackMapper.insert(record);
    }

    public Feedback selectByPrimaryKey(Integer id) {
        return feedbackMapper.selectByPrimaryKey(id);
    }
}
