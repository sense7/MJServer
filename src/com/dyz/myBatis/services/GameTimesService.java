package com.dyz.myBatis.services;

import com.dyz.myBatis.dao.GameTimesMapper;
import com.dyz.myBatis.daoImp.GameTimesDaoImp;
import com.dyz.myBatis.model.GameTimes;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * Created by StanWind on 2017/5/3.
 */
public class GameTimesService {
    private GameTimesMapper gameTimesMapper;

    private static GameTimesService gameTimesService = new GameTimesService();

    public static GameTimesService getInstance() {
        return gameTimesService;
    }

    public void initSetSession(SqlSessionFactory sqlSessionFactory) {
        gameTimesMapper = new GameTimesDaoImp(sqlSessionFactory);
    }


    public int deleteByPrimaryKey(Integer id) {
        return gameTimesMapper.deleteByPrimaryKey(id);

    }

    public int insert(GameTimes record) {
        return gameTimesMapper.insert(record);
    }

    public int insertSelective(GameTimes record) {
        return gameTimesMapper.insertSelective(record);
    }


    public GameTimes selectByUUID(Integer uuid) {
        return gameTimesMapper.selectByUUID(uuid);
    }

    public int updateByUUID(GameTimes record) {
        return gameTimesMapper.updateByUUID(record);
    }

}
