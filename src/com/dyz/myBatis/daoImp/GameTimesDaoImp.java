package com.dyz.myBatis.daoImp;

import com.dyz.myBatis.dao.GameTimesMapper;
import com.dyz.myBatis.model.GameTimes;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * Created by StanWind on 2017/5/3.
 */
public class GameTimesDaoImp implements GameTimesMapper {
    private SqlSessionFactory sqlSessionFactory;

    public GameTimesDaoImp(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public GameTimes selectByUUID(Integer uuid) {
        GameTimes result = null;
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            GameTimesMapper mapper = sqlSession.getMapper(GameTimesMapper.class);
            result = mapper.selectByUUID(uuid);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(GameTimes record) {
        int flag = 0;
        SqlSession sqlSession = sqlSessionFactory.openSession();
        GameTimesMapper mapper = null;
        try {
            mapper = sqlSession.getMapper(GameTimesMapper.class);
            flag = mapper.insert(record);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return flag;
    }

    @Override
    public int insertSelective(GameTimes record) {
        int flag = 0;
        SqlSession sqlSession = sqlSessionFactory.openSession();
        GameTimesMapper mapper = null;
        try {
            mapper = sqlSession.getMapper(GameTimesMapper.class);
            flag = mapper.insert(record);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return flag;
    }


    @Override
    public int updateByUUID(GameTimes record) {
        int flag = 0;
        SqlSession sqlSession = sqlSessionFactory.openSession();
        GameTimesMapper mapper = null;
        try {
            mapper = sqlSession.getMapper(GameTimesMapper.class);
            flag = mapper.updateByUUID(record);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return flag;
    }
}
