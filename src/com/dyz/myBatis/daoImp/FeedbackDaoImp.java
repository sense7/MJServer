package com.dyz.myBatis.daoImp;

import com.dyz.myBatis.dao.FeedbackMapper;
import com.dyz.myBatis.model.Feedback;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * Created by StanWind on 2017/5/16.
 */
public class FeedbackDaoImp implements FeedbackMapper {

    private SqlSessionFactory sqlSessionFactory;

    public FeedbackDaoImp(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(Feedback record) {
        int flag = 0;
        SqlSession sqlSession = sqlSessionFactory.openSession();
        FeedbackMapper mapper = null;
        try {
            mapper = sqlSession.getMapper(FeedbackMapper.class);
            flag = mapper.insert(record);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return flag;
    }

    @Override
    public Feedback selectByPrimaryKey(Integer id) {
        Feedback feedback = null;
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            FeedbackMapper mapper = sqlSession.getMapper(FeedbackMapper.class);
            feedback = mapper.selectByPrimaryKey(id);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return feedback;
    }
}
