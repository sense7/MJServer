package com.dyz.context;

/**
 * 胡牌规则
 *
 * @author luck
 */
public class Rule {
    //充值接口调用允许的IP
    public static String RECHARGE_IP = "127.0.0.1";

    //转转麻将胡牌，普通胡牌


    //长沙麻将胡牌
    public static String Hu_qs_dasixi = "qs_dasixi";
    public static String Hu_qs_banban = "qs_banban";
    public static String Hu_qs_queyise = "qs_queyise";
    public static String Hu_qs_liuliu = "qs_liuliu";
    public static String Hu_qs_ping = "qs_ping"; //平胡

    public static String Hu_pengpeng = "pengpeng";
    public static String Hu_jiangjiang = "jiangjiang";
    public static String Hu_haohuaqxd = "haohuaqxd";
    public static String Hu_qianggang = "qianggang";//抢杠胡
    public static String Hu_quanqiuren = "quanqiuren";

    //公用
    public static String Hu_zi_common = "zi_common";//自摸普通胡
    public static String Hu_other_common = "other_common";//别人自摸普通胡
    public static String Hu_d_self = "d_self";//别人点自己胡
    public static String Hu_d_other = "d_other";//自己点别人胡
    public static String Hu_d_qingyise = "qingyise";//点炮清一色
    public static String Hu_zi_qingyise = "zi_qingyise";//自摸清一色
    public static String Hu_haidilao = "haidilao";
    public static String Hu_haidipao = "haidipao";
    public static String Hu_self_qixiaodui = "self_qixiaodui"; //七小对
    public static String Hu_other_qixiaodui = "other_qixiaodui";
    public static String Hu_gangshangpao = "gangshangpao";
    public static String Hu_gangshanghua = "gangshanghua";
    public static String Hu_tian = "tian";
    public static String Hu_di = "di";

    //杠牌
    public static String Gang_ming = "ming";
    public static String Gang_an = "an";
    public static String Gang_dian = "diangang";
    //划水麻将
    public static String Gang_ming_guolu = "gang_guolu";//过路杠
    public static String Gang_fang = "fanggang";//放杠

    //四川麻将
    public static String Hu_duidui = "duidui";//对对胡
    public static String Hu_yaojiupai = "yaojiupai";// 幺九牌
    public static String Hu_qingqidui = "qingqidui"; //清七对
    public static String Hu_qingdui = "qingdui";// 清对
    public static String Hu_jiangdui = "jiangdui"; //将对


}

