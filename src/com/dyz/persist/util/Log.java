package com.dyz.persist.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by StanWind on 2017/4/10.
 */
public class Log {
    private static Logger logger;

//    public void init() {
//        //因为在引用了mybatis后 会自动先配置一次log4j 这个文件在src目录下
//        //PropertyConfigurator.configure(LOG4JPATH);
//        try {
//            Properties prop = new Properties();
//            prop.load(Log.class.getResourceAsStream(LOG4JPATH));
//            PropertyConfigurator.configure(prop);
//
//
//        } catch (IOException e) {
//            getLogger().error("IOException", e);
//        }
//        //logger = LoggerFactory.getLogger(ServerLog.class);
//    }

    /**
     * 根据上次调用栈返回Logger
     *
     * @return
     */
    public static Logger getLogger() {
//        if (logger == null)
//            init();

        StackTraceElement stack[] = Thread.currentThread().getStackTrace();
        logger = LoggerFactory.getLogger(stack[2].getClassName() + "." + stack[2].getMethodName());
        return logger;
    }
}
