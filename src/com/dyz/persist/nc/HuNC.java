package com.dyz.persist.nc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by StanWind on 2017/4/5.
 */
public class HuNC {
    /**
     * 获取上精牌列表
     *
     * @param paiList       手牌
     * @param zhengJingType 正精牌
     * @return 精牌数组
     */
    public Jing[] GetJingArr(int[] paiList, int zhengJingType) {
        int fuJingType;
        Jing[] shangJingArr = new Jing[2];
        Jing zhengJing = new Jing(zhengJingType, 0);
        if (paiList[zhengJingType] <= 4) {
            zhengJing = new Jing(zhengJingType, paiList[zhengJingType]);
        }

        if (zhengJingType == 8 || zhengJingType == 17 || zhengJingType == 26) {
            fuJingType = zhengJingType - 8;
        } else if (zhengJingType == 33) {
            fuJingType = zhengJingType - 2;
        } else if (zhengJingType == 30) {
            fuJingType = zhengJingType - 3;
        } else {
            fuJingType = zhengJingType + 1;
        }
        Jing fuJing = new Jing(fuJingType, 0);
        if (paiList[fuJingType] <= 4) {
            fuJing = new Jing(fuJingType, paiList[fuJingType]);
        }

        shangJingArr[0] = zhengJing;
        shangJingArr[1] = fuJing;

        return shangJingArr;
    }

    /**
     * 南昌平胡
     *
     * @param paiList
     * @param shangJingArr
     * @return
     */
    public boolean isHu(int[] paiList, Jing[] shangJingArr) {
        int shangJingCount = shangJingArr[0].paiCount + shangJingArr[1].paiCount;
        int[] wan_arr = new int[9];
        int[] tiao_arr = new int[9];
        int[] tong_arr = new int[9];
        int[] feng_arr = new int[4];
        int[] san_arr = new int[3];
        int needNum = 0;
        int index = 0;

        //先将手牌中上精置空，事先通过GetshangJingArr（）存储在了上精列表
        paiList[shangJingArr[0].paiType] = 0;
        paiList[shangJingArr[1].paiType] = 0;

        for (int i = 0; i < 27; i++) {
            if (i < 9) {
                wan_arr[index] = paiList[i];
                index++;
            }
            if (i >= 9 && i < 18) {
                if (i == 9) {
                    index = 0;
                }
                tiao_arr[index] = paiList[i];
                index++;
            }
            if (i >= 18) {
                if (i == 18) {
                    index = 0;
                }
                tong_arr[index] = paiList[i];
                index++;
            }
        }
        for (int i = 27; i < 31; i++) {
            if (i == 27) {
                index = 0;
            }
            feng_arr[index] = paiList[i];
            index++;
        }
        for (int i = 31; i < 34; i++) {
            if (i == 31) {
                index = 0;
            }
            san_arr[index] = paiList[i];
            index++;
        }

        needNum = getNumWithisJiang(wan_arr.clone()) + getNumber(tiao_arr.clone()) + getNumber(tong_arr.clone()) + getNumber(feng_arr.clone()) + getNumber(san_arr.clone());
        if (needNum <= shangJingCount) {
            paiList = null;
            return true;
        } else {
            needNum = getNumber(wan_arr.clone()) + getNumWithisJiang(tiao_arr.clone()) + getNumber(tong_arr.clone()) + getNumber(feng_arr.clone()) + getNumber(san_arr.clone());
            if (needNum <= shangJingCount) {
                paiList = null;
                return true;
            } else {
                needNum = getNumber(wan_arr.clone()) + getNumber(tiao_arr.clone()) + getNumWithisJiang(tong_arr.clone()) + getNumber(feng_arr.clone()) + getNumber(san_arr.clone());

                if (needNum <= shangJingCount) {
                    paiList = null;
                    return true;
                } else {
                    needNum = getNumber(wan_arr.clone()) + getNumber(tiao_arr.clone()) + getNumber(tong_arr.clone()) + getNumWithisJiang(feng_arr.clone()) + getNumber(san_arr.clone());
                    if (needNum <= shangJingCount) {
                        paiList = null;
                        return true;
                    } else {
                        needNum = getNumber(wan_arr.clone()) + getNumber(tiao_arr.clone()) + getNumber(tong_arr.clone()) + getNumber(feng_arr.clone()) + getNumWithisJiang(san_arr.clone());
                        if (needNum <= shangJingCount) {
                            paiList = null;
                            return true;
                        } else {
                            paiList = null;
                            return false;
                        }
                    }
                }
            }
        }
    }

    /**
     * 将牌所在
     *
     * @param arr
     * @return
     */
    private static int getNumWithisJiang(Object arr) {
        boolean isJiang = false;
        int result = 0;
        int[] temp_arr = (int[]) arr;

        for (int i = 0; i < temp_arr.length; i++) {
            if (temp_arr[i] == 7) {
                temp_arr[i] = 1;//该牌碰了扔有一张
            } else if (temp_arr[i] > 4) {
                temp_arr[i] = 0;
            }

        }

        if (temp_arr.length == 9 || temp_arr.length == 3) {
            for (int i = 0; i < temp_arr.length; i++) {
                if (temp_arr[i] > 0) {
                    if ((i < 7 && temp_arr.length == 9) || (i == 0 && temp_arr.length == 3)) {
                        if (temp_arr[i] == 4) {
                            temp_arr[i] = 1;
                            i--;
                        } else if (temp_arr[i] == 3 && (temp_arr[i + 1] == 0 || temp_arr[i + 2] == 0)) {
                            temp_arr[i] = 0;
                        } else if (temp_arr[i] >= 2 && isJiang == false
                                && temp_arr[i + 1] != temp_arr[i]
                                && temp_arr[i + 2] != temp_arr[i]) {
                            //先判断有将牌没有，如果没有将牌，先将这两张牌作将
                            temp_arr[i] -= 2;
                            isJiang = true;
                            i--; //若此处牌不一定为零张时，通过i--再次判断此处位置，如果为零张，则无需再判断
                        } else {
                            if (temp_arr[i + 1] > 0 && temp_arr[i + 2] > 0) {
                                //如果这张牌的下一张和再下一张都不为空的情况。可以组成一级牌
                                temp_arr[i]--;
                                temp_arr[i + 1]--;
                                temp_arr[i + 2]--;
                                i--;
                            } else if (temp_arr[i + 1] > 0 && temp_arr[i + 2] == 0) {
                                //如果这张牌的下一张不为空，再下一张为空，需要一张赖子
                                temp_arr[i]--;
                                temp_arr[i + 1]--;
                                result++;
                                i--;
                            } else if (temp_arr[i + 1] == 0 && temp_arr[i + 2] > 0) {
                                //如果下一张为空，再下一张不为空，先判断有将没有，如果没有将，并且这张牌只有一张，补一张赖子组成将
                                //如果这张牌有两张以上，直接做将，不补赖子。
                                //如果有将的情况，为中间补一张赖子。
                                //检查1-1----1
                                if (temp_arr.length == 9) {
                                    int count = 0;
                                    List<Integer> index = new ArrayList<>();
                                    if (i != 6) {
                                        for (int j = i + 3; j < temp_arr.length; j++) {
                                            if (temp_arr[j] > 0) { //1-1--1--1
                                                index.add(j);
                                                count++;
                                            }
                                        }
                                        //1-1-1-1-1   1-1--1--1
                                        for (int j = 0; j < index.size(); j++) {
                                            if (temp_arr[index.get(j)] == 2 || temp_arr[index.get(j)] == 3) {
                                                count = 1;
                                                break;
                                            }
                                            if (j == 0) {
                                                if (index.get(j) - (i + 2) >= 3) {
                                                    count = 1;
                                                    break;
                                                }
                                            } else {
                                                if (index.get(j) - index.get(j - 1) >= 3) {
                                                    count = 1;
                                                    break;
                                                }
                                            }
                                        }

                                        if (count == 1 || temp_arr[i + 3] != 0) { //1-122----     1-12------
                                            temp_arr[i]--;
                                            temp_arr[i + 2]--;
                                            result++;
                                            i--;
                                        }
                                    }
                                }


                                if (i >= 0 && temp_arr[i] > 0) {
                                    if (isJiang == false) {
                                        if (temp_arr[i] == 1) {
                                            temp_arr[i] = 0;
                                            isJiang = true;
                                            result++;
                                            i--;
                                        } else {
                                            if (temp_arr[i] >= 2) {
                                                isJiang = true;
                                                temp_arr[i] -= 2;
                                                i--;
                                            }
                                        }
                                    } else {
                                        temp_arr[i]--;
                                        temp_arr[i + 2]--;
                                        result++;
                                        i--;
                                    }
                                }
                                //end else if (isJiang == false)
                            }//end if (temp_arr[i + 1] == 0 && temp_arr[i + 2] > 0)
                            else if (temp_arr[i + 1] == 0 && temp_arr[i + 2] == 0) {
                                //如果下一张和再下一张牌都为空的情况，先判断有将没得，如果有将了，为下一张牌补一张赖子
                                //如果还没有将，并且这张牌只有一张，补一张赖子组成将
                                //如果这张牌有两张以上，直接做将，不补赖子。
                                if (isJiang == true) {
                                    if (temp_arr[i] == 1) {
                                        temp_arr[i + 1]++;
                                        result++;
                                        i--;
                                    } else if (temp_arr[i] == 4) {
                                        temp_arr[i] = 1;
                                        i--;
                                    } else {
                                        result = result + 3 - temp_arr[i];
                                        temp_arr[i] = 0;
                                    }
                                } else {
                                    if (temp_arr[i] == 1) {
                                        temp_arr[i] = 0;
                                        isJiang = true;
                                        result++;
                                    } else {
                                        if (temp_arr[i] >= 2) {
                                            isJiang = true;
                                            temp_arr[i] -= 2;
                                            i--;
                                        }
                                    }
                                }
                            }//end if (temp_arr[i + 1] == 0 && temp_arr[i + 2] == 0)
                        }//end else if(temp_arr[i] >= 2 && isJiang == false && temp_arr[i+1] == 0)
                    }//end if((i < 7 && temp_arr.length == 9) || (i == 0 && temp_arr.length == 3))
                    else {
                        //牌面点子大得7的时候，先判断有没有将牌，如果没有，并且这张牌只有一张，补一张赖子组成将
                        //如果这张牌有两张以上，直接做将，不补赖子。
                        //如果有将牌，则判断当前牌和下一张牌是否为空，如果都不为空，在前面补一张赖子
                        //如果下一张牌为空，则将当前牌补成大对，缺多少张补多少张赖子
                        if ((i == 7 && temp_arr.length == 9) || (i == 1 && temp_arr.length == 3)) {
                            if (isJiang == false) {
                                if (temp_arr[i] == 1) {
                                    temp_arr[i] = 0;
                                    result++;
                                    isJiang = true;
                                } else if (temp_arr[i] >= 2) {
                                    temp_arr[i] -= 2;
                                    isJiang = true;
                                    i--;
                                }
                            } else {
                                if (temp_arr[i + 1] > 0) {
                                    temp_arr[i]--;
                                    temp_arr[i + 1]--;
                                    result++;
                                    i--;
                                } else if (temp_arr[i + 1] == 0) {
                                    result = result + 3 - temp_arr[i];
                                    temp_arr[i] = 0;
                                }
                            }
                        }//end if(i == 7 && temp_arr.length == 9)|| (i == 1 && temp_arr.length == 3)
                        else if ((i == 8 && temp_arr.length == 9) || (i == 2 && temp_arr.length == 3)) {
                            if (isJiang == false) {
                                if (temp_arr[i] == 1) {
                                    temp_arr[i] = 0;
                                    result++;
                                    isJiang = true;
                                } else if (temp_arr[i] >= 2) {
                                    temp_arr[i] -= 2;
                                    isJiang = true;
                                    i--;
                                }
                            } else {
                                //如果当前牌为本类牌的最后一张 直接补充为大对
                                result = result + 3 - temp_arr[i];
                                temp_arr[i] = 0;
                            }
                        }//end if(i == 8 && temp_arr.length == 9)|| (i == 2 && temp_arr.length == 3)
                    }
                }//end if(temp_arr[i]>0)
            }//end for
        }//end if(temp_arr.length==9||temp_arr.length == 3)
        else if (temp_arr.length == 4) {
            while (true) {
                List<Integer> indes = new ArrayList<>();
                for (int i = 0; i < temp_arr.length; i++) {
                    if (temp_arr[i] >= 2 && isJiang == false) {
                        temp_arr[i] -= 2;
                        isJiang = true;
                        i--;
                    } else if (temp_arr[i] > 0) {
                        indes.add(i);
                    }
                }
                if (indes.size() >= 3) {
                    for (int j = 0; j < 3; j++) {
                        temp_arr[indes.get(j)]--;
                    }
                } else if (indes.size() == 2) {
                    result++;
                    for (int j = 0; j < indes.size(); j++) {
                        temp_arr[indes.get(j)]--;
                    }
                } else if (indes.size() == 1) {
                    result = result - temp_arr[indes.get(0)] + 3;
                    temp_arr[indes.get(0)] = 0;
                    break;
                } else {
                    break;
                }
            }
        }
        if (isJiang == false) {
            result += 2;
        }
        return result;
    }

    /**
     * 非将牌所在
     *
     * @param arr
     * @return
     */
    private static int getNumber(Object arr) {
        int result = 0;
        int[] temp_arr = (int[]) arr;
        for (int i = 0; i < temp_arr.length; i++) {
            if (temp_arr[i] == 7) {
                temp_arr[i] = 1;//该牌碰了扔有一张
            } else if (temp_arr[i] > 4) {
                temp_arr[i] = 0;
            }

        }

        if (temp_arr.length == 9 || temp_arr.length == 3) {
            for (int i = 0; i < temp_arr.length; i++) {
                if (temp_arr[i] > 0) {
                    if ((i < 7 && temp_arr.length == 9) || (i == 0 && temp_arr.length == 3)) {
                        if (temp_arr[i + 1] > 0 && temp_arr[i + 2] > 0) {
                            if (temp_arr[i] == 3) {
                                temp_arr[i] = 0;
                            } else {
                                temp_arr[i]--;
                                temp_arr[i + 1]--;
                                temp_arr[i + 2]--;
                                i--;
                            }
                        } else if (temp_arr[i + 1] > 0 && temp_arr[i + 2] == 0) {
                            if (temp_arr[i] == 3) {
                                temp_arr[i] = 0;
                            } else {
                                temp_arr[i]--;
                                temp_arr[i + 1]--;
                                result++;
                                i--;
                            }

                        } else if (temp_arr[i + 1] == 0 && temp_arr[i + 2] > 0) {
                            if (temp_arr[i] == 3) {
                                temp_arr[i] = 0;
                            } else {
                                temp_arr[i]--;
                                temp_arr[i + 2]--;
                                result++;
                                i--;
                            }
                        } else if (temp_arr[i + 1] == 0 && temp_arr[i + 2] == 0) {
                            if (temp_arr[i] == 3) {
                                temp_arr[i] = 0;
                            } else if (temp_arr[i] == 2) {
                                temp_arr[i] = 0;
                                result++;
                            } else if (temp_arr[i] == 1) {
                                temp_arr[i + 1]++;
                                result++;
                                i--;
                            }
                        }
                    }//end if((i < 7 && temp_arr.length == 9) || (i == 0 && temp_arr.length == 3))
                    else {
                        if ((i == 7 && temp_arr.length == 9) || (i == 1 && temp_arr.length == 3)) {
                            if (temp_arr[i + 1] > 0) {
                                temp_arr[i]--;
                                temp_arr[i + 1]--;
                                result++;
                                i--;
                            } else if (temp_arr[i + 1] == 0) {
                                result = result + 3 - temp_arr[i];
                                temp_arr[i] = 0;
                            }
                        } else if ((i == 8 && temp_arr.length == 9) || (i == 2 && temp_arr.length == 3)) {
                            result = result + 3 - temp_arr[i];
                            temp_arr[i] = 0;
                        }
                    }
                }
            }
        }//end if (temp_arr.length == 9 || temp_arr.length == 3)
        else if (temp_arr.length == 4) {
            while (true) {
                List<Integer> indexs = new ArrayList<>();
                for (int i = 0; i < temp_arr.length; i++) {
                    if (temp_arr[i] > 0) {
                        indexs.add(i);
                    }
                }
                if (indexs.size() >= 3) {
                    for (int j = 0; j < 3; j++) {
                        temp_arr[indexs.get(j)]--;
                    }
                } else if (indexs.size() == 2) {
                    result++;
                    for (int j = 0; j < indexs.size(); j++) {
                        temp_arr[indexs.get(j)]--;
                    }
                } else if (indexs.size() == 1) {
                    result = result + 3 - temp_arr[indexs.get(0)];
                    temp_arr[indexs.get(0)] = 0;
                    break;
                } else {
                    break;
                }
            }


        }//end if(temp_arr.length==4)

        return result;
    }

    /**
     * 判断是否七小对子
     *
     * @param paiList
     * @param shangJingArr
     * @param isMenQing
     * @return
     */
    public boolean checkSevenDouble(int[] paiList, Jing[] shangJingArr, boolean isMenQing) {

        if (!isMenQing) {
            paiList = null;
            return false;
        }

        int shangJingCount = shangJingArr[0].paiCount + shangJingArr[1].paiCount;

        //先将手牌中上精置空，事先通过GetshangJingArr（）存储在了上精列表
        paiList[shangJingArr[0].paiType] = 0;
        paiList[shangJingArr[1].paiType] = 0;

        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] != 0) {
                if (paiList[i] != 2 && paiList[i] != 4) {
                    shangJingCount--;
                    if (shangJingCount < 0 || paiList[i] > 4)//>4 û������
                    {
                        paiList = null;
                        return false;
                    }
                }
            }
        }
        paiList = null;
        return true;
    }

    /**
     * 检查是否十三烂胡牌
     *
     * @param paiList
     * @param shangJingArr
     * @param isMenQing
     * @return 0-没有十三烂 1-十三烂 2-七星十三烂
     */
    public int checkThirteenOrphans(int[] paiList, Jing[] shangJingArr, boolean isMenQing) {

        if (!isMenQing) {
            paiList = null;
            return 0;
        }

        int result = 2;
        //先判断字牌，七星十三烂
        for (int i = 27; i < paiList.length; i++) {
            if (paiList[i] == 0) {
                result = 1;
            } else {
                paiList[i]--;  //有一种字牌有多张，可能字牌为精牌的情况
            }
        }

        //先将手牌中上精置空，事先通过GetJingArr（）存储在了上精列表
        paiList[shangJingArr[0].paiType] = 0;
        paiList[shangJingArr[1].paiType] = 0;

        for (int i = 27; i < paiList.length; i++) {
            if (paiList[i] > 0) {//说明字牌不为精牌，且有多余字牌
                paiList = null;
                return 0;
            }
        }

        //将万条筒分三段判断
        for (int j = 0; j < 3; j++) {
            int preType = -2;
            for (int i = 0; i < 9; i++) {
                if (paiList[i + j * 9] != 0) {
                    if (i - preType < 3) {
                        paiList = null;
                        return 0;
                    } else {
                        preType = i;
                        paiList[i + j * 9]--;
                        i--;
                    }
                }
            }
        }
        paiList = null;
        return result;
    }

    /**
     * 判断是否是碰碰胡
     *
     * @param paiList
     * @param shangJingArr
     * @return
     */
    public boolean checkPengPengHu(int[] paiList, Jing[] shangJingArr) {
        //先判断手牌数量，防止有吃
        int paiCount = 0;
        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] <= 4) {
                paiCount += paiList[i];
            } else if (paiList[i] == 6 || paiList[i] == 7) {
                paiCount += paiList[i] - 3;
            } else if (paiList[i] == 5) {
                paiCount += 3;
            }
        }
        if (paiCount != 14) {
            paiList = null;
            return false;
        }

        boolean isJiang = false;
        int shangJingCount = shangJingArr[0].paiCount + shangJingArr[1].paiCount;

        //先将手牌中上精置空，事先通过GetshangJingArr（）存储在了上精列表
        paiList[shangJingArr[0].paiType] = 0;
        paiList[shangJingArr[1].paiType] = 0;

        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] >= 3) {
                paiList[i] = 0;
            }
        }

        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] > 0) {
                if (paiList[i] == 1) {
                    if (isJiang == false) {
                        shangJingCount--;
                        isJiang = true;
                    } else {
                        shangJingCount -= 2;
                    }
                } else {
                    if (isJiang == false) {
                        isJiang = true;
                    } else {
                        shangJingCount--;
                    }
                }
                if (shangJingCount < 0) {
                    paiList = null;
                    return false;
                }
            }
        }
        paiList = null;
        return true;
    }

    /**
     * 判断是否是精吊平胡
     *
     * @param paiList
     * @param shangJingArr
     * @param lastPaiType
     * @return
     */
    public boolean checkJingDiao(int[] paiList, Jing[] shangJingArr, int lastPaiType) {
        //先将听得牌从手牌中移除
        paiList[lastPaiType]--;

        //将手中精牌清空
        paiList[shangJingArr[0].paiType] = 0;
        paiList[shangJingArr[1].paiType] = 0;

        int shangJingCount = shangJingArr[0].paiCount + shangJingArr[1].paiCount;
        if (lastPaiType == shangJingArr[0].paiType || lastPaiType == shangJingArr[1].paiType) {
            shangJingCount--;
        }

        if (shangJingCount == 0) {
            paiList = null;
            return false;
        }


        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] == 7) {
                paiList[i] = 1;
            } else if (paiList[i] > 4) {
                paiList[i] = 0;
            }

        }

        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] > 0) {
                if ((i < 27 && i % 9 < 7) || i == 31) {
                    if (paiList[i + 1] > 0 && paiList[i + 2] > 0) {
                        paiList[i]--;
                        paiList[i + 1]--;
                        paiList[i + 2]--;
                        i--;
                    } else if (paiList[i + 1] > 0 && paiList[i + 2] == 0) {
                        if (paiList[i] == 3) {
                            paiList[i] = 0;
                        } else {
                            shangJingCount--;
                            if (shangJingCount < 1) {
                                paiList = null;
                                return false;
                            } else {
                                paiList[i]--;
                                paiList[i + 1]--;
                                i--;
                            }
                        }
                    } else if (paiList[i + 1] == 0 && paiList[i + 2] > 0) {
                        if (paiList[i] == 3) {
                            paiList[i] = 0;
                        } else {
                            shangJingCount--;
                            if (shangJingCount < 1) {
                                paiList = null;
                                return false;
                            } else {
                                paiList[i]--;
                                paiList[i + 2]--;
                                i--;
                            }
                        }
                    } else if (paiList[i + 1] == 0 && paiList[i + 2] == 0) {
                        shangJingCount = shangJingCount + paiList[i] - 3;
                        if (shangJingCount < 1) {
                            paiList = null;
                            return false;
                        } else {
                            paiList[i] = 0;
                        }
                    }
                } else if ((i < 27 && i % 9 == 7) || i == 32) {
                    if (paiList[i] == 3) {
                        paiList[i] = 0;
                    } else {
                        if (paiList[i + 1] > 0) {
                            shangJingCount--;
                            if (shangJingCount < 1) {
                                paiList = null;
                                return false;
                            } else {
                                paiList[i]--;
                                paiList[i + 1]--;
                                i--;
                            }
                        } else {
                            shangJingCount = shangJingCount + paiList[i] - 3;
                            if (shangJingCount < 1) {
                                paiList = null;
                                return false;
                            } else {
                                paiList[i] = 0;
                            }
                        }
                    }
                } else if ((i < 27 && i % 9 == 8) || i == 33) {
                    shangJingCount = shangJingCount + paiList[i] - 3;
                    if (shangJingCount < 1) {
                        paiList = null;
                        return false;
                    } else {
                        paiList[i] = 0;
                    }
                } else if (i >= 27 && i <= 30) {
                    List<Integer> indexs = new ArrayList<>();
                    for (int j = 27; j < 31; j++) {
                        if (paiList[j] > 0) {
                            indexs.add(j);
                        }
                    }
                    if (indexs.size() >= 3) {
                        for (int j = 0; j < 3; j++) {
                            paiList[indexs.get(j)]--;
                        }
                    } else if (indexs.size() == 2) {
                        for (int j = 0; j < indexs.size(); j++) {
                            shangJingCount = shangJingCount - 3 + paiList[indexs.get(j)];
                            paiList[indexs.get(j)] = 0;
                        }
                    } else if (indexs.size() == 1) {
                        shangJingCount = shangJingCount - 3 + paiList[indexs.get(0)];
                        paiList[indexs.get(0)] = 0;
                        break;
                    } else {
                        break;
                    }
                }//end if(i==27)
            }//end if(paiList[i]>0)
        }
        if (shangJingCount == 1) {
            paiList = null;
            return true;
        } else {
            paiList = null;
            return false;
        }

    }

    /**
     * 判断是否精吊七小对
     *
     * @param paiList
     * @param shangJingArr
     * @param lastPaiType
     * @param isMenQing
     * @return 0-没有精吊七小对  1-精吊七小对 2-精吊德国七小对
     */
    public int checkJingDiaoSevenDouble(int[] paiList, Jing[] shangJingArr, int lastPaiType, boolean isMenQing) {
        if (!isMenQing) {
            paiList = null;
            return 0;
        }

        //先将听得牌从手牌中移除
        paiList[lastPaiType]--;


        //将手中精牌清空
        paiList[shangJingArr[0].paiType] = 0;
        paiList[shangJingArr[1].paiType] = 0;

        int shangJingCount = shangJingArr[0].paiCount + shangJingArr[1].paiCount;
        if (lastPaiType == shangJingArr[0].paiType || lastPaiType == shangJingArr[1].paiType) {
            shangJingCount--;
        }

        if (shangJingCount == 0) //原本手牌没有精牌
        {
            paiList = null;
            return 0;
        }

        int result = 2;

        //判断精吊七小对
        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] > 0) {
                if (paiList[i] != 2 && paiList[i] != 4) {
                    shangJingCount--;
                    if (shangJingCount < 1 || paiList[i] > 4) {
                        paiList = null;
                        return 0;
                    }
                }
            }
        }

        //将最后一张牌加入
        paiList[lastPaiType]++;
        paiList[shangJingArr[0].paiType] = shangJingArr[0].paiCount;
        paiList[shangJingArr[1].paiType] = shangJingArr[1].paiCount;

        //判断德国
        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] > 0) {
                if (paiList[i] != 2 && paiList[i] != 4) {
                    result = 1;
                }
            }
        }

        paiList = null;
        return result;
    }

    /**
     * 判断是否精吊碰碰胡
     *
     * @param paiList
     * @param shangJingArr
     * @param lastPaiType
     * @return 0-没有精吊碰碰胡 1-精吊碰碰胡 2-精吊德国碰碰胡
     */
    public int checkJingDiaoPengPengHu(int[] paiList, Jing[] shangJingArr, int lastPaiType) {
        int paiCount = 0;
        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] <= 4) {
                paiCount += paiList[i];
            } else if (paiList[i] == 6 || paiList[i] == 7) {
                paiCount += paiList[i] - 3;
            }
        }
        if (paiCount != 14) {
            paiList = null;
            return 0;
        }
        //先将听得牌从手牌中移除
        paiList[lastPaiType]--;

        boolean isJiang = false;
        int shangJingCount = shangJingArr[0].paiCount + shangJingArr[1].paiCount;
        if (lastPaiType == shangJingArr[0].paiType || lastPaiType == shangJingArr[1].paiType) {
            shangJingCount--;
        }

        if (shangJingCount == 0) //原本手牌没有精牌
        {
            paiList = null;
            return 0;
        }

        //先将手牌中上精置空，事先通过GetshangJingArr（）存储在了上精列表
        paiList[shangJingArr[0].paiType] = 0;
        paiList[shangJingArr[1].paiType] = 0;

        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] >= 3) {
                paiList[i] = 0;
            }

        }

        int result = 2;

        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] > 0) {
                if (paiList[i] == 1) {
                    if (isJiang == false) {
                        shangJingCount--;
                        isJiang = true;
                    } else {
                        shangJingCount -= 2;
                    }
                } else {
                    if (isJiang == false) {
                        isJiang = true;
                    } else {
                        shangJingCount--;
                    }
                }
                if (shangJingCount < 1) {
                    paiList = null;
                    return 0;
                }
            }
        }

        isJiang = false;

        //将最后一张牌加入
        paiList[lastPaiType]--;
        paiList[shangJingArr[0].paiType] = shangJingArr[0].paiCount;
        paiList[shangJingArr[1].paiType] = shangJingArr[1].paiCount;
        //判断德国
        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] > 0) {
                if (paiList[i] != 3) {
                    if (paiList[i] == 1) {
                        result = 1;
                    } else {
                        if (isJiang == false) {
                            isJiang = true;
                        } else {
                            result = 1;
                        }
                    }
                }
            }
        }

        paiList = null;
        return result;

    }


    boolean isFirst = true;

    /**
     * 平胡(德国)
     *
     * @param paiList
     * @return
     */
    public boolean isDeGuoHu(int[] paiList) {

        boolean isJiang = false;

        if (isFirst) {
            for (int i = 0; i < paiList.length; i++) {
                if (paiList[i] == 7) {
                    paiList[i] = 1;
                } else if (paiList[i] > 4) {
                    paiList[i] = 0;
                }

            }
            isFirst = false;
        }

        if (Remain(paiList) == 0) {
            paiList = null;
            return true;           //递归退出条件：如果没有剩牌，则胡牌返回。
        }

        for (int i = 0; i < paiList.length; i++) // 找到有牌的地方，i就是当前牌,   PAI[i]是个数
        {
            //   跟踪信息
            if (paiList[i] > 0) {
                //  4张组合(杠子)
                if (paiList[i] == 4)                               //   如果当前牌数等于4张
                {
                    paiList[i] = 0;                                //   除开全部4张牌
                    if (isDeGuoHu(paiList)) {
                        paiList = null;
                        return true;             // 如果剩余的牌组合成功，和牌
                    }
                    paiList[i] = 4;                                     // 否则，取消4张组合
                }
                //   3张组合(大对)
                if (paiList[i] >= 3)                               //   如果当前牌不少于3张
                {
                    paiList[i] -= 3;                                   //   减去3张牌
                    if (isDeGuoHu(paiList)) {
                        paiList = null;
                        return true;             //   如果剩余的牌组合成功，胡牌
                    }
                    paiList[i] += 3;                                   //  取消3张组合
                }
                //     2张组合(将牌)
                if (isJiang == false && paiList[i] >= 2)               //  如果之前没有将牌，且当前牌不少于2张
                {
                    isJiang = true;                                    //  设置将牌标志־
                    paiList[i] -= 2;                                   //   减去2张牌
                    if (isDeGuoHu(paiList)) {
                        paiList = null;
                        return true;        //   如果剩余的牌组合成功，胡牌
                    }
                    paiList[i] += 2;                                   //   取消2张组合
                    isJiang = false;                                   //   清除将牌标志־
                }
                if (i < 27) {
                    //   顺牌组合，注意是从前往后组合！
                    //   排除数值为8和9的牌
                    if (i % 9 != 7 && i % 9 != 8 && paiList[i + 1] != 0 && paiList[i + 2] != 0)    //  如果后面有连续两张牌
                    {
                        paiList[i]--;
                        paiList[i + 1]--;
                        paiList[i + 2]--;             // 各牌数减1
                        if (isDeGuoHu(paiList)) {
                            paiList = null;
                            return true;             //  如果剩余的牌组合成功，胡牌
                        }
                        paiList[i]++;
                        paiList[i + 1]++;
                        paiList[i + 2]++;                                     //  恢复各牌数
                    }
                } else if (i == 31) {
                    if (paiList[i + 1] != 0 && paiList[i + 2] != 0) {
                        paiList[i]--;
                        paiList[i + 1]--;
                        paiList[i + 2]--;             //   各牌数减1
                        if (isDeGuoHu(paiList)) {
                            paiList = null;
                            return true;             //   如果剩余的牌组合成功，胡牌
                        }
                        paiList[i]++;
                        paiList[i + 1]++;
                        paiList[i + 2]++;
                    }
                } else if (i > 26 && i < 31) {
                    List<Integer> indexs = new ArrayList<>();
                    for (int j = 27; j < 31; j++) {
                        if (paiList[j] > 0) {
                            indexs.add(j);
                        }
                    }
                    if (indexs.size() == 3) {
                        for (int a = 0; a < indexs.size(); a++) {
                            paiList[indexs.get(a)]--;
                        }
                        if (isDeGuoHu(paiList)) {
                            paiList = null;
                            return true;
                        }
                        for (int a = 0; a < indexs.size(); a++) {
                            paiList[indexs.get(a)]++;
                        }
                    }
                }//end if(i>26&&i<31)
                else if (i == 32 || i == 33) {
                    paiList = null;
                    return false;
                }
            }//end if(paiList[i] > 0)
        }//end for
        //  无法全部组合，不胡！
        paiList = null;
        return false;
    }

    /**
     * 检查剩余牌数
     *
     * @param paiList
     * @return
     */
    private int Remain(int[] paiList) {
        int sum = 0;
        for (int i = 0; i < paiList.length; i++) {
            sum += paiList[i];
        }
        return sum;
    }

    /**
     * 判断是否德国七小对
     *
     * @param paiList
     * @param isMenQing
     * @return
     */
    public boolean checkDeGuoSevenDouble(int[] paiList, boolean isMenQing) {
        if (!isMenQing) {
            paiList = null;
            return false;
        }

        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] > 0) {
                if (paiList[i] != 2 && paiList[i] != 4) {
                    paiList = null;
                    return false;
                }
            }
        }
        paiList = null;
        return true;
    }

    /**
     * 检查是否十三烂胡牌，
     *
     * @param paiList
     * @param isMenQing
     * @return 0-没有十三烂 1-十三烂 2-七星十三烂
     */
    public int checkDeGuoThirteenOrphans(int[] paiList, boolean isMenQing) {
        if (!isMenQing) {
            paiList = null;
            return 0;
        }

        int result = 2;


        //将万条筒分三段判断
        for (int j = 0; j < 3; j++) {
            int preType = -3;
            for (int i = 0; i < 9; i++) {
                if (paiList[i + j * 9] != 0) {
                    if (i - preType < 3) {
                        paiList = null;
                        return 0;
                    } else {
                        preType = i;
                        paiList[i + j * 9]--;
                        i--;
                    }
                }
            }
        }

        //判断字牌，七星十三烂
        for (int i = 27; i < paiList.length; i++) {
            if (paiList[i] == 0) {
                result = 1;
            } else if (paiList[i] > 1) {
                result = 0;
            }
        }

        paiList = null;
        return result;
    }

    /**
     * 判断是否是碰碰胡(德国)
     *
     * @param paiList
     * @return
     */
    public boolean checkDeGuokPengPengHu(int[] paiList) {
        //先判断手牌数量，防止有吃
        int paiCount = 0;
        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] <= 4) {
                paiCount += paiList[i];
            } else if (paiList[i] == 6 || paiList[i] == 7) {
                paiCount += paiList[i] - 3;
            } else if (paiList[i] == 5) {
                paiCount += 3;
            }
        }
        if (paiCount != 14) {
            paiList = null;
            return false;
        }

        boolean isJiang = false;
        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] >= 3) {
                paiList[i] = 0;
            }
        }

        for (int i = 0; i < paiList.length; i++) {
            if (paiList[i] > 0) {
                if (paiList[i] == 1) {
                    paiList = null;
                    return false;
                } else {
                    if (isJiang == false) {
                        isJiang = true;
                    } else {
                        paiList = null;
                        return false;
                    }
                }
            }
        }

        paiList = null;
        return true;
    }
}
