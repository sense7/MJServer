package com.dyz.persist.nc;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by StanWind on 2017/4/5.
 */
public class Test {
    public static void main(String[] args) {
//        int[] pai_arr = {1, 0, 0, 1, 0, 0, 0, 0, 0, //万
//                0, 0, 0, 1, 0, 1, 0, 1, 0, //条
//                0, 0, 1, 0, 0, 1, 0, 0, 0, //筒
//                1, 1, 1, 1,           //风
//                1, 1, 1};            //大三元
        int[] pai_arr = {0, 1, 0, 0, 1, 0, 0, 1, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0};
        HuNCTypeScore majhong = new HuNCTypeScore();
        JingScore getJingScore = new JingScore();

        //上精数组
        Jing[] shangjingArr = majhong.GetJingArr(pai_arr, 21);

        //胡牌类型
        int typeOfHu = majhong.GetTypeOfHu(pai_arr, shangjingArr, 9, 5, true);

        int[] idPlayer = new int[]{0, 1, 2, 3};
        int idBanker = 1;
        int idWiner = 1;
        int idDianpao = 3;
        boolean isDeZhongDe = false;
        int huMode = 5;

        //胡牌类型的分数
        Map<Integer, Integer> huTypeScore = majhong.GetScoreOfHu(typeOfHu, huMode, idBanker, isDeZhongDe, idPlayer, idWiner, idDianpao);


        int[] p1 = new int[]{2, 1};
        int[] p2 = new int[]{0, 0};
        int[] p3 = new int[]{0, 0};
        int[] p4 = new int[]{0, 0};

        Map<Integer, int[]> jingMap = new HashMap<>();
        jingMap.put(0, p1);
        jingMap.put(1, p2);
        jingMap.put(2, p3);
        jingMap.put(3, p4);

        //精牌分数
        // Map<Integer, Integer> jingScore=getJingScore.GetJingScore(jingMap);


        System.out.println("胡牌类型:" + typeOfHu);
        System.out.println("score hu" + huTypeScore);
        // System.out.println("score jing"+jingScore);

    }

}

