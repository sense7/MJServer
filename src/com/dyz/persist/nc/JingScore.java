package com.dyz.persist.nc;

import java.util.HashMap;
import java.util.Map;

public class JingScore {
    /**
     * 冲关分
     *
     * @param jingMap 四个玩家的精牌数组
     * @return
     */
    private static Map<Integer, String> GetChongGuanScore(Map<Integer, Jing[]> jingMap) {
        Map<Integer, String> jingChongGuanScore = new HashMap<>();

        for (Integer ID : jingMap.keySet()) {
            jingChongGuanScore.put(ID, "0:0");
        }

        for (Map.Entry<Integer, Jing[]> entry : jingMap.entrySet()) {
            int score = entry.getValue()[0].paiCount * 2 + entry.getValue()[1].paiCount;
            //冲关
            if (score >= 5) {
                score = score * (score - 3);
                jingChongGuanScore.put(entry.getKey(), (Integer.parseInt(jingChongGuanScore.get(entry.getKey()).split(":")[0]) + score * 3) + ":" + "1");
            } else {
                jingChongGuanScore.put(entry.getKey(), (Integer.parseInt(jingChongGuanScore.get(entry.getKey()).split(":")[0]) + score * 3) + ":" + "0");
            }

            for (int ID : jingChongGuanScore.keySet()) {
                if (ID != entry.getKey()) {
                    jingChongGuanScore.put(ID, (Integer.parseInt(jingChongGuanScore.get(ID).split(":")[0]) - score) + ":" + jingChongGuanScore.get(ID).split(":")[1]);
                }
            }
            System.out.println("123" + jingChongGuanScore.toString());
        }

        return jingChongGuanScore;
    }

    /**
     * 霸王 冲关分*2
     *
     * @param jingScore
     * @param jingMap
     * @return
     */
    private static Map<Integer, String> GetBaWangScore(Map<Integer, Jing[]> jingMap, Map<Integer, String> jingScore) {

        int count = 0;
        for (Map.Entry<Integer, Jing[]> entry : jingMap.entrySet()) {
            if ((entry.getValue()[0].paiCount + entry.getValue()[1].paiCount) > 0) {
                count++;
            }
        }

        //霸王
        if (count == 1) {
            for (Map.Entry<Integer, String> entry : jingScore.entrySet()) {
                if (Integer.parseInt(entry.getValue().split(":")[0]) > 0) {
                    jingScore.put(entry.getKey(), Integer.parseInt(entry.getValue().split(":")[0]) * 2 + ":" + entry.getValue().split(":")[1] + ":" + "1");
                } else {
                    jingScore.put(entry.getKey(), Integer.parseInt(entry.getValue().split(":")[0]) * 2 + ":" + entry.getValue().split(":")[1] + ":" + "0");
                }
            }
        } else {
            for (Map.Entry<Integer, String> entry : jingScore.entrySet()) {
                jingScore.put(entry.getKey(), jingScore.get(entry.getKey()) + ":" + "0");
            }
        }

        return jingScore;
    }

    public static Map<Integer, String> GetJingScore(Map<Integer, Jing[]> jingMap) {
        return GetBaWangScore(jingMap, GetChongGuanScore(jingMap));
    }
}

