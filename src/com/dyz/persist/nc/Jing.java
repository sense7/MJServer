package com.dyz.persist.nc;

/**
 * Created by StanWind on 2017/4/5.
 */
public class Jing {
    //默认Jing数组 0-正精 1-副精 2-下精
    public int paiType;
    public int paiCount;

    public Jing(int paiType, int paiCount) {
        this.paiType = paiType;
        this.paiCount = paiCount;
    }

    @Override
    public String toString() {
        return "Jing{" +
                "paiType=" + paiType +
                ", paiCount=" + paiCount +
                '}';
    }
}
