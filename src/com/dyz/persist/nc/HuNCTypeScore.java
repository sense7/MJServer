package com.dyz.persist.nc;

import com.dyz.gameserver.Avatar;
import com.dyz.persist.util.GlobalUtil;
import com.dyz.persist.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class HuNCTypeScore extends HuNC {
    private int[] m_paiList;

    public int[] getPaiList() {
        return m_paiList.clone();
    }

    public void setPaiList(int[] paiList) {
        if (paiList != null)
            m_paiList = paiList;
    }

    public static int GetTypeOfHu(int[] paiList, int zhengJingPoint, int lastPaiType, int huMode, boolean isMenQing) {
        HuNCTypeScore majhong = new HuNCTypeScore();
        //上精数组
        Jing[] shangjingArr = majhong.GetJingArr(paiList, zhengJingPoint);
        //胡牌类型
//	        Log.getLogger().info("胡牌输入参数->" + paiList
//	                + "\n上张牌->" + lastPaiType + "\t胡牌方式->" + huMode
//	                + "\t门清->" + isMenQing + "\t正精->" + zhengJingPoint);
        return majhong.GetTypeOfHu(paiList, shangjingArr, lastPaiType, huMode, isMenQing);

    }

    /**
     * 二维手牌处理
     *
     * @param avatar    处理的玩家
     * @param handleChi 是否处理吃牌(去掉) 进入胡牌会处理吃 进入精牌不会处理吃
     * @return
     */
    public static int[] getOneListPai(Avatar avatar, boolean handleChi) {
        int[][] paiList = avatar.getPaiArray();
        //不需要移除掉碰，杠了的牌组，在判断是否胡的时候就应判断了
        //paiList  = cleanGangAndPeng(paiList,avatar);

        int[] pai_arr = GlobalUtil.CloneIntList(paiList[0]);

        int[] chiL = avatar.getChiNotMovePai();
        //Log.getLogger().info("ava->" + avatar.avatarVO.getAccount().toString());
        Log.getLogger().info(Arrays.toString(pai_arr));
        //杠牌变5 吃牌去掉 碰牌+3  也就是牌
        ///碰 1  杠2  胡3  吃4
        for (int i = 0; i < paiList[0].length; i++) {
            //TODO 是否处理吃牌
            if (handleChi)
                pai_arr[i] -= chiL[i];

            if (paiList[1][i] == 2 && pai_arr[i] == 4) {
                pai_arr[i] = 5;
            }

            if (paiList[1][i] == 1 && pai_arr[i] >= 3) {
                pai_arr[i] += 3;
            }
        }
        Log.getLogger().info("---------------处理后-----------------");
        Log.getLogger().info(Arrays.toString(pai_arr));
        return pai_arr;
    }


    /**
     * 获取胡牌类型
     *
     * @param paiList       手牌
     * @param shangjingList 上精
     * @param lastPaiType   上一张牌
     * @param huMode        胡牌方式 0-天胡 1-地胡 2-杠上开花 3-抢杠胡 4-自摸 5-放炮
     * @return 返回胡牌类型 0-没有胡牌 1-南昌平胡 2-碰碰胡 3-七小对 4-十三烂 5-七星十三烂
     * 6-德国平胡 7-德国碰碰胡 8-德国七小对 9-德国十三烂 10-德国七星十三烂
     * 11-精吊平胡 12-精吊德国平胡 13-精吊七小对 14-精吊德国七小对 15-精吊碰碰胡 16-精吊德国碰碰胡
     */
    public int GetTypeOfHu(int[] paiList, Jing[] shangjingList, int lastPaiType, int huMode, boolean isMenQing) {
        setPaiList(paiList);
        int type = 0;

        if (checkSevenDouble(getPaiList(), shangjingList, isMenQing)) //七小对
        {
            type = 3;
            if (checkJingDiaoSevenDouble(getPaiList(), shangjingList, lastPaiType, isMenQing) != 0) //精吊七小对
            {
                if (checkJingDiaoSevenDouble(getPaiList(), shangjingList, lastPaiType, isMenQing) == 1) {
                    type = 13;
                } else if (checkJingDiaoSevenDouble(getPaiList(), shangjingList, lastPaiType, isMenQing) == 2) // 精吊德国七小对
                {
                    type = 14;
                }
            } else if (checkDeGuoSevenDouble(getPaiList(), isMenQing))         //德国七小对
            {
                type = 8;
            }
        } else if (checkThirteenOrphans(getPaiList(), shangjingList, isMenQing) != 0) {
            if (checkThirteenOrphans(getPaiList(), shangjingList, isMenQing) == 2) //七星十三烂
            {
                type = 5;
                if (checkDeGuoThirteenOrphans(getPaiList(), isMenQing) == 2) {//德国七星十三烂
                    type = 10;
                }
            } else                                                //十三烂
            {
                type = 4;
                if (checkDeGuoThirteenOrphans(getPaiList(), isMenQing) == 1)    //德国十三烂
                {
                    type = 9;
                }
            }
        } else if (isHu(getPaiList(), shangjingList)) //南昌平胡
        {
            type = 1;
            if (checkJingDiao(getPaiList(), shangjingList, lastPaiType) && huMode == 4)  //精吊&&自摸
            {
                type = 11;
                if (isDeGuoHu(getPaiList()))//精吊德国平胡
                {
                    type = 12;
                    if (checkJingDiaoPengPengHu(getPaiList(), shangjingList, lastPaiType) == 2)//精吊德国碰碰胡
                    {
                        type = 16;
                    }
                } else if (checkJingDiaoPengPengHu(getPaiList(), shangjingList, lastPaiType) == 1)//精吊碰碰胡
                {
                    type = 15;
                }
            } else if (isDeGuoHu(getPaiList()))  //德国平胡
            {
                type = 6;
                if (checkDeGuokPengPengHu(getPaiList())) //德国碰碰胡
                {

                    type = 7;
                }
            } else if (checkPengPengHu(getPaiList(), shangjingList)) //碰碰胡
            {
                type = 2;
            }
        }

        String name = "";
        switch (type) {
            case 0:
                name = "没胡";
                break;
            case 1:
                name = "南昌平胡";
                break;
            case 2:
                name = "碰碰胡";
                break;
            case 3:
                name = "七小对";
                break;
            case 4:
                name = "十三烂";
                break;
            case 5:
                name = "七星十三烂";
                break;
            case 6:
                name = "德国平胡";
                break;
            case 7:
                name = "德国碰碰胡";
                break;
            case 8:
                name = "德国七小对";
                break;
            case 9:
                name = "德国十三烂";
                break;
            case 10:
                name = "德国七星十三烂";
                break;
            case 11:
                name = "精吊平胡";
                break;
            case 12:
                name = "精吊德国平胡";
                break;
            case 13:
                name = "精吊七小对";
                break;
            case 14:
                name = "精吊德国七小对";
                break;
            case 15:
                name = "精吊碰碰胡";
                break;
            case 16:
                name = "精吊德国碰碰胡";
                break;
        }
        //Console.WriteLine("Name: " + name);
        Log.getLogger().info("Name: " + name);
        return type;
    }

    /**
     * 胡牌者的胡牌分
     *
     * @param typeOfHu    要求>0 0-没有胡牌 1-南昌平胡 2-碰碰胡 3-七小对 4-十三烂 5-七星十三烂
     *                    6-德国平胡 7-德国碰碰胡 8-德国七小对 9-德国十三烂  10-德国七星十三烂
     *                    11-精吊平胡 12-精吊德国平胡 13-精吊七小对 14-精吊德国七小对 15-精吊碰碰胡 16-精吊德国碰碰胡
     * @param huMode      0-天胡 1-地胡 2-杠上开花 3-抢杠胡 4-自摸 5-点炮
     * @param idBanker
     * @param isDeZhongDe
     * @param idPlayer
     * @param idWiner
     * @param idDianpao
     * @return
     */
    public static Map<Integer, Integer> GetScoreOfHu(int typeOfHu, int huMode, int idBanker, boolean isDeZhongDe, int[] idPlayer, int idWiner, int idDianpao) {
        Map<Integer, Integer> scoreMap = new HashMap<>();
        for (int i = 0; i < idPlayer.length; i++) {
            scoreMap.put(idPlayer[i], 0);
        }

        if (typeOfHu == 0) {
            return null;
        }

        Integer newScore = 0;
        //胡牌类型
        switch (typeOfHu) {
            case 1:
                newScore++;
                break;
            case 2:
            case 3:
            case 4:
            case 6:
            case 11:
                newScore += 2;
                break;
            case 5:
            case 7:
            case 8:
            case 9:
            case 12:
            case 13:
            case 15:
                newScore += 4;
                break;
            case 10:
            case 14:
            case 16:
                newScore += 8;
                break;
            default:
                break;
        }


        //胡牌方式
        switch (huMode) {
            //天胡 地胡
            case 0:
            case 1:
                newScore = 20;
                if (typeOfHu >= 11 || typeOfHu <= 16) //精吊
                {
                    newScore *= 2;
                }
                scoreMap.put(idWiner, newScore * 3);
                for (int i = 0; i < idPlayer.length; i++) {
                    if (idPlayer[i] != idWiner) {
                        scoreMap.put(idPlayer[i], -1 * newScore);
                    }
                }
                break;
            //杠上开花
            case 2:
                newScore *= 4;
                //庄家赢
                if (idWiner == idBanker) {
                    //庄家翻倍
                    newScore *= 2;
                    if ((typeOfHu >= 6 && typeOfHu <= 10) || typeOfHu == 12 || typeOfHu == 14 || typeOfHu == 16) {
                        if (isDeZhongDe) //德中德
                        {
                            newScore *= 2;
                        }
                        newScore += 5;    //德国+5
                    }
                    scoreMap.put(idWiner, newScore * 3);
                    for (int i = 0; i < idPlayer.length; i++) {
                        if (idPlayer[i] != idWiner) {
                            scoreMap.put(idPlayer[i], -1 * newScore);
                        }
                    }
                } else {
                    //非庄家赢
                    if ((typeOfHu >= 6 && typeOfHu <= 10) || typeOfHu == 12 || typeOfHu == 14 || typeOfHu == 16) {
                        if (isDeZhongDe) //德中德
                        {
                            newScore *= 2;
                        }
                        newScore += 5;    //德国+5
                        scoreMap.put(idWiner, newScore * 4 - 5);//庄家输得德国的+5不翻倍

                        for (int i = 0; i < idPlayer.length; i++) {
                            if (idPlayer[i] == idBanker) {
                                scoreMap.put(idBanker, -1 * newScore * 2 + 5);
                            } else if (idPlayer[i] != idWiner) {
                                scoreMap.put(idPlayer[i], -1 * newScore);
                            }
                        }

                    } else {
                        scoreMap.put(idWiner, newScore * 4);

                        for (int i = 0; i < idPlayer.length; i++) {
                            if (idPlayer[i] == idBanker) {
                                scoreMap.put(idBanker, -1 * newScore * 2);
                            } else if (idPlayer[i] != idWiner) {
                                scoreMap.put(idPlayer[i], -1 * newScore);
                            }
                        }

                    }


                }
                break;
            //抢杠 自摸
            case 3:
            case 4:
                newScore *= 2;
                //庄家赢
                if (idWiner == idBanker) {
                    //庄家赢翻倍
                    newScore *= 2;
                    if ((typeOfHu >= 6 && typeOfHu <= 10) || typeOfHu == 12 || typeOfHu == 14 || typeOfHu == 16) {
                        if (isDeZhongDe) //德中德
                        {
                            newScore *= 2;
                        }
                        newScore += 5;    //德国+5
                    }
                    scoreMap.put(idWiner, newScore * 3);
                    for (int i = 0; i < idPlayer.length; i++) {
                        if (idPlayer[i] != idWiner) {
                            scoreMap.put(idPlayer[i], -1 * newScore);
                        }
                    }
                } else {
                    //非庄家赢
                    if ((typeOfHu >= 6 && typeOfHu <= 10) || typeOfHu == 12 || typeOfHu == 14 || typeOfHu == 16) {
                        if (isDeZhongDe) //德中德
                        {
                            newScore *= 2;
                        }
                        newScore += 5;
                        scoreMap.put(idWiner, newScore * 4 - 5);//庄家输的德国的+5不翻倍

                        for (int i = 0; i < idPlayer.length; i++) {
                            if (idPlayer[i] == idBanker) {
                                scoreMap.put(idBanker, -1 * newScore * 2 + 5);
                            } else if (idPlayer[i] != idWiner) {
                                scoreMap.put(idPlayer[i], -1 * newScore);
                            }
                        }


                    } else {
                        scoreMap.put(idWiner, newScore * 4);
                        for (int i = 0; i < idPlayer.length; i++) {
                            if (idPlayer[i] == idBanker) {
                                scoreMap.put(idBanker, -1 * newScore * 2);
                            } else if (idPlayer[i] != idWiner) {
                                scoreMap.put(idPlayer[i], -1 * newScore);
                            }
                        }
                    }

                }
                break;
            //点炮
            case 5:
                //庄家赢
                if (idWiner == idBanker) {
                    newScore *= 2;
                    if ((typeOfHu >= 6 && typeOfHu <= 10) || typeOfHu == 12 || typeOfHu == 14 || typeOfHu == 16) {
                        if (isDeZhongDe) //德中德
                        {
                            newScore *= 2;
                        }
                        newScore += 5;    //德国+5
                        scoreMap.put(idWiner, newScore * 4 - 5); //点炮者输的德国的+5不翻倍

                        for (int i = 0; i < idPlayer.length; i++) {
                            if (idPlayer[i] == idDianpao) {
                                scoreMap.put(idPlayer[i], -1 * newScore * 2 + 5);
                            } else if (idPlayer[i] != idWiner) {
                                scoreMap.put(idPlayer[i], -1 * newScore);
                            }
                        }

                    } else {
                        //非德国
                        scoreMap.put(idWiner, newScore * 4);

                        for (int i = 0; i < idPlayer.length; i++) {
                            if (idPlayer[i] == idDianpao) {
                                scoreMap.put(idDianpao, -1 * newScore * 2);
                            } else if (idPlayer[i] != idWiner) {
                                scoreMap.put(idPlayer[i], -1 * newScore);
                            }
                        }

                    }

                } else {
                    //非庄家赢
                    if ((typeOfHu >= 6 && typeOfHu <= 10) || typeOfHu == 12 || typeOfHu == 14 || typeOfHu == 16) {
                        if (isDeZhongDe) //德中德
                        {
                            newScore *= 2;
                        }
                        newScore += 5;    //德国+5
                        //庄家点炮
                        if (idBanker == idDianpao) {
                            scoreMap.put(idWiner, newScore * 6 - 5 * 3); //庄家点炮德国的+5也不翻倍

                            for (int i = 0; i < idPlayer.length; i++) {
                                if (idPlayer[i] == idBanker) {
                                    scoreMap.put(idPlayer[i], -1 * newScore * 4 + 5 * 3);
                                } else if (idPlayer[i] != idWiner && idPlayer[i] != idBanker) {
                                    scoreMap.put(idPlayer[i], -1 * newScore);
                                }
                            }
                        } else {
                            scoreMap.put(idWiner, newScore * 5 - 5 * 2); //庄家和点炮者德国的+5也不翻倍

                            for (int i = 0; i < idPlayer.length; i++) {
                                if (idPlayer[i] == idBanker || idPlayer[i] == idDianpao) {
                                    scoreMap.put(idPlayer[i], -1 * newScore * 2 + 5);
                                } else if (idPlayer[i] != idWiner) {
                                    scoreMap.put(idPlayer[i], -1 * newScore);
                                }
                            }
                        }

                    } else {
                        //非德国
                        //庄家点炮
                        if (idBanker == idDianpao) {
                            scoreMap.put(idWiner, newScore * 6);

                            for (int i = 0; i < idPlayer.length; i++) {
                                if (idPlayer[i] == idBanker) {
                                    scoreMap.put(idBanker, -1 * newScore * 4);
                                } else if (idPlayer[i] != idWiner) {
                                    scoreMap.put(idPlayer[i], -1 * newScore);
                                }
                            }
                        } else {
                            //非庄家点炮
                            scoreMap.put(idWiner, newScore * 5);

                            for (int i = 0; i < idPlayer.length; i++) {
                                if (idPlayer[i] == idBanker || idPlayer[i] == idDianpao) {
                                    scoreMap.put(idPlayer[i], -1 * newScore * 2);
                                } else if (idPlayer[i] != idWiner) {
                                    scoreMap.put(idPlayer[i], -1 * newScore);
                                }
                            }

                        }
                    }
                }
                break;
        }

        return scoreMap;
    }
}
