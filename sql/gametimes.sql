SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gametimes`
-- ----------------------------
DROP TABLE IF EXISTS `gametimes`;
CREATE TABLE `gametimes` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `gameid` int(100) NOT NULL COMMENT '分配给游戏的id',
  `uuid` int(100) NOT NULL COMMENT '玩家UUID',
  `win` int(100) NOT NULL DEFAULT '0' COMMENT '胜利场次',
  `lose` int(100) NOT NULL DEFAULT '0' COMMENT '失败场次',
  `draw` int(100) NOT NULL DEFAULT '0' COMMENT '平局场次',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

