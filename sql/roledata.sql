/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : roledata

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-04-17 20:28:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Uuid` int(11) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `nickName` varchar(255) DEFAULT NULL,
  `headIcon` varchar(255) DEFAULT NULL,
  `roomCard` int(11) DEFAULT NULL,
  `unionid` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `prizecount` int(11) DEFAULT NULL,
  `manager_up_id` int(11) DEFAULT NULL,
  `actualCard` int(11) DEFAULT NULL,
  `totalCard` int(11) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT NULL,
  `lastLoginTime` timestamp NULL DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `isGame` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1101 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('1050', '100000', 'oqbNexIWZfo7mSTJyYU8h4tsuVIE', '爷爷', 'http://wx.qlogo.cn/mmopen/2BIpRg0mria32k0L5mjcqkiaqX4MxhniautQ0wVlCDiauB8gYzU81UeqIdJ88etWKwlaYIdDKWXeK5mOaqGdjV6qLdhZjFc6AR98/0', '12733', 'oXvowv5QmtPVxDV973RdUUcT17io', '', '', '2', '0', null, '5', '11738', '2017-03-01 16:51:42', null, '0', '1');
INSERT INTO `account` VALUES ('1051', '101050', 'oqbNexF9jw-ixDai7RDX4hxkzelc', '柯芬', 'http://wx.qlogo.cn/mmopen/OzX4zAA7WtWIjAsmhHmibah4prm7ZVtYfriaBU2AjQWG8hWlqpgXhSmTYYxmAaT4suqaaghWjwFWxYZZuyNHIprFk9EzJQUvBL/0', '1000', 'oXvowv0GiseMo47TXIssyFU_dAXE', 'Jiangxi', 'Jiujiang', '2', '0', null, '5', '5', '2017-03-01 16:54:47', null, '0', '1');
INSERT INTO `account` VALUES ('1052', '101051', 'oqbNexAB5zLv3ySxMgf0Ri0rNHIU', '_', 'http://wx.qlogo.cn/mmopen/PiajxSqBRaEIu2nWiabBYXyX1znFIXQiaicLicI93sc9nKVuUgBCO4jqarLJ9wtZXvgmDOhVPNAbMgr3ib6g6sXHthNw/0', '301', 'oXvowv1hHiWOq0kYT5qO6mZQ8WnU', 'Jiangxi', 'Pingxiang', '1', '1', null, '5', '800', '2017-03-01 17:03:33', null, '0', '1');
INSERT INTO `account` VALUES ('1053', '101052', 'oqbNexBvXinqBflK8LGvQJDgkvws', 'abh', 'http://wx.qlogo.cn/mmopen/OzX4zAA7WtWIjAsmhHmibaiaNfl01JPPc88hIhIWtdCclKWu8KpPsl4kUia0ZJyAnvIye00mlFKTfOQruMC0Z5100PqgOtuyURC/0', '118', 'oXvowv9yG3gTwQSz4urRU69Ha6DQ', '', '', '1', '0', null, '5', '119', '2017-03-01 17:08:56', null, '0', '1');
INSERT INTO `account` VALUES ('1054', '101053', 'oqbNexC2Y5lP4zjpiFvCvXQ_B4Zg', '魏勤伟', 'http://wx.qlogo.cn/mmopen/2BIpRg0mria1ibuk0CjsVyW3DvBW5r9d1wcpFn22S5OXmhalEX8yyTCoub3S5icdFQ0Uh5ibQAnoA2SzMzmuuUic2QgGgia0rLQ3cl/0', '5', 'oXvowv0UUdY8_VM8hd71i_Y_bg18', '', '', '0', '1', null, '5', '5', '2017-03-04 09:08:51', null, '0', '1');
INSERT INTO `account` VALUES ('1056', '101055', 'oqbNexF9jw-ixDai7RDX4hxkzelc', '柯芬', 'http://wx.qlogo.cn/mmopen/OzX4zAA7WtWIjAsmhHmibah4prm7ZVtYfriaBU2AjQWG8hWlqpgXhSmTYYxmAaT4suqaaghWjwFWxYZZuyNHIprFk9EzJQUvBL/0', '5', 'oXvowv0GiseMo47TXIssyFU_dAXE', 'Jiangxi', 'Jiujiang', '2', '1', null, '5', '5', '2017-03-17 14:48:30', null, '0', '0');
INSERT INTO `account` VALUES ('1057', '101056', 'oqbNexC2Y5lP4zjpiFvCvXQ_B4Zg', '魏勤伟', 'http://wx.qlogo.cn/mmopen/2BIpRg0mria1ibuk0CjsVyW3DvBW5r9d1wcpFn22S5OXmhalEX8yyTCoub3S5icdFQ0Uh5ibQAnoA2SzMzmuuUic2QgGgia0rLQ3cl/0', '5', 'oXvowv0UUdY8_VM8hd71i_Y_bg18', '', '', '0', '1', null, '5', '5', '2017-03-18 12:17:10', null, '0', '0');
INSERT INTO `account` VALUES ('1058', '101057', 'oqbNexKk7WZrCvBjZvGq4p1DGHoI', '言笑晏晏', 'http://wx.qlogo.cn/mmopen/jywBfIouoiabN0vuz3NsPNvcEoqZv1PY0icBjL5Q51NGbg0NBzdB271n9AFPZvry61HqXPb12aqxesn2UKFrgdQd2iaBGm1qQP0/0', '5', 'oXvowvzwrC4b7_KnzNmYIRsblsAE', '', '', '1', '1', null, '5', '5', '2017-03-18 16:06:20', null, '0', '0');
INSERT INTO `account` VALUES ('1059', '101058', 'oqbNexAB5zLv3ySxMgf0Ri0rNHIU', '_', 'http://wx.qlogo.cn/mmopen/PiajxSqBRaEIu2nWiabBYXyX1znFIXQiaicLicI93sc9nKVuUgBCO4jqarLJ9wtZXvgmDOhVPNAbMgr3ib6g6sXHthNw/0', '5', 'oXvowv1hHiWOq0kYT5qO6mZQ8WnU', 'Jiangxi', 'Pingxiang', '1', '1', null, '5', '5', '2017-03-19 09:53:35', null, '0', '0');
INSERT INTO `account` VALUES ('1060', '101059', 'oqbNexNCMrD9gjnUGDw7jHnPYA5c', 'HelloWorld', 'http://wx.qlogo.cn/mmopen/OzX4zAA7WtWIjAsmhHmibavvKUfwFNf9DqyS7kzYyTVPicd2DNBF5UFIY47na1j71KiaUznfSN1dVnHUiaQMJtCBTmIAribNQ0mQQ/0', '5', 'oXvowv63upU0oI83TmXd6GJymz0o', 'Jiangxi', 'Ganzhou', '1', '1', null, '5', '5', '2017-03-20 08:38:24', null, '0', '0');
INSERT INTO `account` VALUES ('1061', '101060', 'oqbNexJSr25S1vMvs7RvtVfck8WM', '’', 'http://wx.qlogo.cn/mmopen/2BIpRg0mria3WXMgZuBxaicTpazmFLo1sv6fiahCVuxKqOCpibSmTPXHgUEh807pszqfh9uFficjG2eOHhbPibIJbjDpCyicxTNibCE2/0', '5', 'oXvowv5g1bpRfhFnvT-fCldzcS-8', 'Jiangxi', 'Nanchang', '2', '1', null, '5', '5', '2017-03-20 21:49:01', null, '0', '0');
INSERT INTO `account` VALUES ('1088', '101087', 'oqbNexDBAX8kEdez6WB57KihECeE', 'JY', '', '5', 'oXvowv8VRK-1-cmW_pTElEuti6zM', '', '', '0', '1', null, '5', '2222', '2017-04-05 16:57:10', null, '0', '0');
INSERT INTO `account` VALUES ('1093', '101092', 'oqbNexHxXooP-bvkPCLNlJEgKZ0w', '康惠聪', 'http://wx.qlogo.cn/mmopen/2BIpRg0mria0cmkRjLFGLHib3e2o8Isj2jibFbiaWmXa2A1DgtzZEr6ibvZU6WRibomYuCDR4dxmttLCHl5m3btJ4TZiabXkRXm53Dy/0', '5', 'oXvowv_IgaajmowPO-01u551g7Mg', 'Jiangxi', 'Nanchang', '1', '1', null, '5', '52222', '2017-04-10 17:05:24', null, '0', '1');
INSERT INTO `account` VALUES ('1094', '101093', 'player02', 'player02', null, '5', 'dfewgfgwr23', 'province', 'city', '1', '1', null, '5', '5222', '2017-04-12 22:56:49', null, '0', '1');
INSERT INTO `account` VALUES ('1095', '101094', 'player04', 'player04', null, '5', 'dfewfgwr23', 'province', 'city', '1', '1', null, '5', '5222', '2017-04-13 09:52:08', null, '0', '1');
INSERT INTO `account` VALUES ('1096', '101095', 'player01', 'player01', null, '5', 'dfewwr23', 'province', 'city', '2', '1', null, '5', '5222', '2017-04-13 10:24:16', null, '0', '1');
INSERT INTO `account` VALUES ('1097', '101096', 'player04', 'player04', null, '5', 'dfewfgwr23', 'province', 'city', '1', '1', null, '5', '52222', '2017-04-14 14:14:50', null, '0', '0');
INSERT INTO `account` VALUES ('1098', '101097', 'player03', 'player03', null, '5', 'dfvfewwr23', 'province', 'city', '2', '1', null, '5', '5222', '2017-04-14 18:01:16', null, '0', '1');
INSERT INTO `account` VALUES ('1099', '101098', 'player04', 'player04', null, '5', 'dfewfgwr23', 'province', 'city', '1', '1', null, '5', '52222', '2017-04-15 08:56:10', null, '0', '1');
INSERT INTO `account` VALUES ('1100', '101099', 'player01', 'player01', null, '5', 'dfewwr23', 'province', 'city', '2', '1', null, '5', '522', '2017-04-17 09:37:26', null, '0', '0');

-- ----------------------------
-- Table structure for `contactway`
-- ----------------------------
DROP TABLE IF EXISTS `contactway`;
CREATE TABLE `contactway` (
  `id` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contactway
-- ----------------------------
INSERT INTO `contactway` VALUES ('1', 'contact with tel 130');

-- ----------------------------
-- Table structure for `game`
-- ----------------------------
DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `id` int(11) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT NULL,
  `endtTime` timestamp NULL DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `status` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of game
-- ----------------------------

-- ----------------------------
-- Table structure for `gameaccountindex`
-- ----------------------------
DROP TABLE IF EXISTS `gameaccountindex`;
CREATE TABLE `gameaccountindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `accountIndex` int(1) NOT NULL,
  `cardList` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PK_GAME_INDEX_ID` (`game_id`),
  KEY `PK_ACCOUNT_INDEX_ID` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gameaccountindex
-- ----------------------------

-- ----------------------------
-- Table structure for `gamerecord`
-- ----------------------------
DROP TABLE IF EXISTS `gamerecord`;
CREATE TABLE `gamerecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `type` char(1) NOT NULL,
  `cardIndex` varchar(11) NOT NULL,
  `acountindex_id` int(11) NOT NULL,
  `curentTime` datetime NOT NULL,
  `playerList_one` varchar(255) NOT NULL,
  `playerList_two` varchar(255) NOT NULL,
  `playerList_three` varchar(255) NOT NULL,
  `playerList_four` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `PK_GAMERECORD_GAME_ID` (`game_id`),
  KEY `PK_GAMERECORD_ACCOUNTINDEX_ID` (`acountindex_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gamerecord
-- ----------------------------

-- ----------------------------
-- Table structure for `manager`
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `power_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `telephone` varchar(11) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `actualCard` int(11) NOT NULL DEFAULT '0',
  `totalCards` int(11) NOT NULL DEFAULT '0',
  `manager_up_id` int(11) DEFAULT '1',
  `status` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `PK_MANANGER_MANAGER_ID` (`manager_up_id`),
  KEY `PK_MANAGER_POWER_M_ID` (`power_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES ('1', '1', 'admin', '18888888888', 'e10adc3949ba59abbe56e057f20f883e', '1', '1', '0', '0');
INSERT INTO `manager` VALUES ('2', '3', 'daili01', '13029898853', '4297f44b13955235245b2497399d7a93', '1108778', '1121111', '1', '0');
INSERT INTO `manager` VALUES ('3', '2', 'daili02', '13029992828', '4297f44b13955235245b2497399d7a93', '2000000', '2000000', '1', '0');

-- ----------------------------
-- Table structure for `noticetable`
-- ----------------------------
DROP TABLE IF EXISTS `noticetable`;
CREATE TABLE `noticetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of noticetable
-- ----------------------------
INSERT INTO `noticetable` VALUES ('19', '哈哈哈哈哈哈哈', '0');

-- ----------------------------
-- Table structure for `operatelog`
-- ----------------------------
DROP TABLE IF EXISTS `operatelog`;
CREATE TABLE `operatelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `manager_down_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `createTime` datetime NOT NULL,
  `mark` varchar(255) DEFAULT NULL,
  `type` char(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PK_OPERATELOG_MANAGER` (`manager_id`),
  KEY `PK_OPERATELOG_MANAGER_DOWN` (`manager_down_id`),
  KEY `PK_OPERATELOG_ACCOUNT` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operatelog
-- ----------------------------

-- ----------------------------
-- Table structure for `paylog`
-- ----------------------------
DROP TABLE IF EXISTS `paylog`;
CREATE TABLE `paylog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_manager_Id` int(11) DEFAULT NULL,
  `reced_Id` int(11) DEFAULT NULL,
  `receType` int(11) DEFAULT NULL,
  `payCount` int(11) DEFAULT NULL,
  `payTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PK_PAYLOG_MANAGER_ID` (`send_manager_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paylog
-- ----------------------------

-- ----------------------------
-- Table structure for `playrecord`
-- ----------------------------
DROP TABLE IF EXISTS `playrecord`;
CREATE TABLE `playrecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playrecord` longtext,
  `standingsDetail_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PK_STANDINGSDETAIL_PLAYRECORDID` (`standingsDetail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of playrecord
-- ----------------------------

-- ----------------------------
-- Table structure for `power`
-- ----------------------------
DROP TABLE IF EXISTS `power`;
CREATE TABLE `power` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of power
-- ----------------------------

-- ----------------------------
-- Table structure for `prize`
-- ----------------------------
DROP TABLE IF EXISTS `prize`;
CREATE TABLE `prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index_id` int(11) NOT NULL,
  `prize_name` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `probability` int(6) NOT NULL DEFAULT '100',
  `status` char(1) NOT NULL DEFAULT '0',
  `prizecount` int(11) NOT NULL DEFAULT '1',
  `type` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prize
-- ----------------------------
INSERT INTO `prize` VALUES ('1', '1', 'Apple大礼包', '../images/prize201703201489993931213.png', '1000', '0', '1', '1');
INSERT INTO `prize` VALUES ('2', '2', '雷电转接器', 'images/prize10.jpg', '9000', '0', '1', '1');

-- ----------------------------
-- Table structure for `prizerule`
-- ----------------------------
DROP TABLE IF EXISTS `prizerule`;
CREATE TABLE `prizerule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `precount` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prizerule
-- ----------------------------
INSERT INTO `prizerule` VALUES ('1', '抽奖联系sss', '1', '0');

-- ----------------------------
-- Table structure for `roominfo`
-- ----------------------------
DROP TABLE IF EXISTS `roominfo`;
CREATE TABLE `roominfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gameType` char(1) DEFAULT NULL,
  `isHong` char(1) DEFAULT '0',
  `roomid` int(11) DEFAULT NULL,
  `sevenDouble` char(1) DEFAULT '0',
  `ma` int(11) DEFAULT NULL,
  `zimo` char(1) DEFAULT NULL,
  `xiayu` int(2) DEFAULT '0',
  `addWordCard` char(1) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `cardNumb` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=993 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roominfo
-- ----------------------------

-- ----------------------------
-- Table structure for `standings`
-- ----------------------------
DROP TABLE IF EXISTS `standings`;
CREATE TABLE `standings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomid` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `createTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PK_ROOM_STANDINGS_ID` (`roomid`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of standings
-- ----------------------------

-- ----------------------------
-- Table structure for `standingsaccountrelation`
-- ----------------------------
DROP TABLE IF EXISTS `standingsaccountrelation`;
CREATE TABLE `standingsaccountrelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `standings_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PK_STANDINGS_ACCOUNT_ID` (`standings_id`),
  KEY `PK_ACCOUNT_STANDINGS_ID` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=917 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of standingsaccountrelation
-- ----------------------------

-- ----------------------------
-- Table structure for `standingsdetail`
-- ----------------------------
DROP TABLE IF EXISTS `standingsdetail`;
CREATE TABLE `standingsdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=363 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of standingsdetail
-- ----------------------------

-- ----------------------------
-- Table structure for `standingsrelation`
-- ----------------------------
DROP TABLE IF EXISTS `standingsrelation`;
CREATE TABLE `standingsrelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `standings_id` int(11) DEFAULT NULL,
  `standingsDetail_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of standingsrelation
-- ----------------------------

-- ----------------------------
-- Table structure for `techargerecord`
-- ----------------------------
DROP TABLE IF EXISTS `techargerecord`;
CREATE TABLE `techargerecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `manager_up_id` int(11) NOT NULL DEFAULT '0',
  `createtime` datetime NOT NULL,
  `techargeMoney` int(11) NOT NULL,
  `mark` varchar(255) DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `PK_TECHARGEREORD_ACCOUNT_ID` (`account_id`),
  KEY `PK_TECHARGEREORD_MANAGER_ID` (`manager_id`),
  KEY `PK_TECHARGEREORD_MANAGER_UP_ID` (`manager_up_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of techargerecord
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `f_id` int(11) NOT NULL DEFAULT '0',
  `f_name` varchar(255) DEFAULT NULL,
  `f_phonenumber` varchar(255) DEFAULT NULL,
  `f_email` varchar(255) DEFAULT NULL,
  `f_passwd` varchar(255) DEFAULT NULL,
  `f_regdate` datetime DEFAULT NULL,
  PRIMARY KEY (`f_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '123', '123@123', 'e10adc3949ba59abbe56e057f20f883e', null);

-- ----------------------------
-- Table structure for `winnersinfo`
-- ----------------------------
DROP TABLE IF EXISTS `winnersinfo`;
CREATE TABLE `winnersinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prize_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `awardTime` datetime DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  `mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `PK_PRIZE_WINNERSINFO_ID` (`prize_id`),
  KEY `PK_ACCOUNT_WINNERSINFO_ID` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of winnersinfo
-- ----------------------------
